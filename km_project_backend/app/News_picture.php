<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News_picture extends Model
{
    protected $table = 'news_picture';

    public function news()
    {
        return $this->belongsTo(News::class, 'news_id', 'id');
    }
}
