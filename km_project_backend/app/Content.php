<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';
    protected $fillable = ['type_id', 'title' , 'detail', 'status'];



    public function content_type()
    {
        return $this->belongsTo(Content_type::class, 'type_id', 'id');
    }

    public function content_picture()
    {
        return $this->hasMany(Content_picture::class);
    }
}
