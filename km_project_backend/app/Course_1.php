<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    
        protected $table = 'courses';
        protected $fillable = ['user_id', 'title' , 'detail', 'path_pic' , 'status'];

    
}
