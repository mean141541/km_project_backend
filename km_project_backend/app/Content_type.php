<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content_type extends Model
{
    protected $table = 'content_type';

    public function content()
    {
        return $this->hasMany(Content::class)->using('Content_picture');
    }
}
