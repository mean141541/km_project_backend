<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses_comment extends Model
{
    protected $table = 'courses_comments';

    public function courses_vdo()
    {
        return $this->belongsTo(Courses_vdo::class, 'course_vdo_id', 'id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
