<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses_vdo extends Model
{
    protected $table = 'courses_vdo';
    public function courses()
    {
        return $this->belongsTo(Courses::class, 'course_id', 'id');
    }
    public function courses_comment()
    {
        return $this->hasMany(Courses_comment::class);
    }
}
