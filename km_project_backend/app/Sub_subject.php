<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_subject extends Model
{
    protected $table = 'sub_subject';
    protected $fillable = [
        'title','detail','subject_id','pic_path','view','tag','status'
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }
}
