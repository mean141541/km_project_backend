<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config';
    protected $fillable = [
        'id','path_pic','address','office_hour','contect_mail',
        'contect_phone','status',
    ];
    protected $hidden = [
        'remember_token',
    ];

    // public static function scopeStatus($query, $status)
    // {
    //     return $query->where([
    //         'status' => $status,
    //     ])->orderBy('created_at', 'desc')->get();
    // }

}
