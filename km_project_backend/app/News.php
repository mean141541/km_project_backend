<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    public function news_type()
    {
        return $this->belongsTo(News_type::class, 'type_id', 'id');
    }

    public function news_picture()
    {
        return $this->hasMany(News_picture::class);
    }
}
