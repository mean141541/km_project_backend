<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model  
{ 
    protected $table = 'comment';

    public function sub_subject()
    {
        return $this->belongsTo(Sub_subject::class, 'sub_subject_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
