<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\Cookie;

class RdrMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Session::get('token'));

        if((Session::get('token')) == null){
            abort(403,'Does not exist token');
        }else{
            try {
                $data = unserialize(base64_decode(Session::get('token')));

            } catch (\Exception $th) {
                abort(403,'Does not exist token');
            }
            return $next($request);
        }
    }
}
