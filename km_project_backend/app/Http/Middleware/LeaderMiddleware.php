<?php

namespace App\Http\Middleware;

use Closure;

class LeaderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }

            if ($data['permission'] == 'Admin' ||
            $data['permission'] == 'Leader'
            ){
                $validate = User::where('email',$data['email'])->get()->first();
                if(!$validate){
                    return response()->json([
                        'message' => 'Unauthorization',
                        'data'=>$data
                    ],401);
                }
                if(!($validat0e->permission == 'Leader' && $validate->permission == 'Admin')){
                    return response()->json([
                        'message' => 'Unauthorization',
                        'data'=>$data
                    ],401);
                }
                return $next($request);
            }else{
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$data
                ],401);
            }
        }
    }
}
