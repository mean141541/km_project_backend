<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubsubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'title' => 'required',
            // 'subject_id' => 'required',
            // 'detail' => 'required',
            // 'status' => 'required',
            // 'tag' => 'required',
            // 'pic_path' => 'required',
        ];
    }
}
