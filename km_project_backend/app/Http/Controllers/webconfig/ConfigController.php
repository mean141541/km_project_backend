<?php

namespace App\Http\Controllers\webconfig;

use App\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Config_log;

class ConfigController extends Controller
{
    private static function keep_log($user_id,$config_id,$action)
    {
        $log_comment = new Config_log();
        $log_comment->user_id = $user_id;
        $log_comment->config_id = $config_id;
        $log_comment->action = $action;
        $log_comment->save();
        return $log_comment;
    }

    public function Config ()
    {
        return view('Admin.ConfigWeb.configWeb',[
            'config'=> Config::all()->first()
        ]);
    }

    public function selectConfig(Config $config)
    {
        // dd($config);
        return view('Admin.ConfigWeb.edit',[
            'config'=>$config
        ]);
    }

    public function add_contract(Request $request)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
            $check_current_contract_count = DB::select("SELECT count(id) AS counts FROM config WHERE status = 'active'");
            if (!$check_current_contract_count[0]->counts == 0){
                return response([
                    'message'=>'contract default already exist',
                    'data'=>null
                ],422);
            }else{
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                    'address' => 'required|string|max:199',
                    'office_hour' => 'required|string|max:49',
                    'contect_mail' => 'required|string|max:49',
                    'contect_phone' => 'required|string|max:99'
                ]);
                $type_content_image = 'config_';
                $count_image_content = 1;
                $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
                $request->image->move(public_path('img/config'), $imageName);
                $file_path_to_database = '/img/config/'.$imageName;

                $config = new Config();
                $config->address = $request->address;
                $config->office_hour = $request->office_hour;
                $config->contect_mail = $request->contect_mail;
                $config->contect_phone = $request->contect_phone;
                $config->path_pic = $file_path_to_database;
                $config->status = 'active';
                $config->save();
                $user_id = User::where('email',$data['email'])->get()->first()->id;
                ConfigController::keep_log($user_id,$config->id,'Create config');
                return response([
                    'message'=>'success',
                    'data'=>[
                        'address'=>$request->address,
                        'office_hour'=>$request->office_hour,
                        'contect_mail'=>$request->contect_mail,
                        'contect_phone'=>$request->contect_phone
                        ]
                    ],201);
            }

    }

    public function configEdit($id)
    {
        $config = DB::select('SELECT * FROM config WHERE id = ?',[$id]);
        return view('Admin.ConfigWeb.edit',['config'=>$config]);
    }

    public function configUpdate(Request $request,Config $config)
    {
        // dd($request->image);
        if($request->image != null){
            $type_content_image = 'config_';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
            $request->image->move(public_path('img/config'), $imageName);
            $file_path_to_database = '/img/config/'.$imageName;
            $config->path_pic = $file_path_to_database;
        }

        $config->address = $request->address;
        $config->office_hour = $request->office_hour;
        $config->contect_mail = $request->contect_mail;
        $config->contect_phone = $request->contect_phone;
        $config->status = $request->status;
        if (!$config->save()) {
            return redirect()->back();
        }
        ConfigController::keep_log(123,$config->id,'Update config');

        return redirect()->route('config.index')->withErrors(['update success']);
    }


}
