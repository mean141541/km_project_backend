<?php

namespace App\Http\Controllers\webconfig;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Config_log;
use App\Config;

class EditConfigController extends Controller
{

    private static function keep_log($user_id,$config_id,$action)
    {
        $log_comment = new Config_log();
        $log_comment->user_id = $user_id;
        $log_comment->config_id = $config_id;
        $log_comment->action = $action;
        $log_comment->save();
        return $log_comment;
    }
    
    public function edit(Request $request)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        // try{
            $request->validate([
                'address' => 'required|string|max:199',
                'office_hour' => 'required|string|max:49',
                'contect_mail' => 'required|string|max:49',
                'contect_phone' => 'required|string|max:99'
            ]);
            DB::update("UPDATE config SET address = ? , office_hour = ? , contect_mail = ? , contect_phone = ? , updated_at = ?",[$request->address,$request->office_hour,$request->contect_mail,$request->contect_phone,date('Y-m-d H:i:s')]);
            $user_id = User::where('email',$data['email'])->get()->first()->id;
            $config = Config::where('status','active')->get()->first();
            EditConfigController::keep_log($user_id,$config->id,'Edit config detail from "address = '.$config->address.', office_hour = '.$config->office_hour.', contect_mail = '.$config->contect_mail.', contect_phone = '.$config->contect_phone.'" to "address = '.$request->address.', office_hour = '.$request->office_hour.', contect_mail = '.$request->contect_mail.', contect_phone = '.$request->contect_phone.'"');
            return response([
                'message'=>'success',
                'data'=>[
                    'address'=>$request->address,
                    'office_hour'=>$request->office_hour,
                    'contect_mail'=>$request->contect_mail,
                    'contect_phone'=>$request->contect_phone
                    ]
                ],200);
        // } catch (\Exception $th){
        //     return response([
        //         'message'=>'error',
        //         'data'=>'service is not available'
        //     ],500);
        // }
    }

    public function edit_photo(Request $request)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        try{
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            ]);
            $news_type_image = 'config_';
            $count_image = 1;
            $imageName = $news_type_image.date('YmdHis').'_'.$count_image.'.'.$request->image->extension();
            $request->image->move(public_path('img/config'), $imageName);
            $file_path_to_database = '/img/config/'.$imageName;
            DB::update('UPDATE config SET path_pic = ?',[$file_path_to_database]);
            $user_id = User::where('email',$data['email'])->get()->first()->id;
            $config = Config::where('status','active')->get()->first();
            EditConfigController::keep_log($user_id,$config->id,'Edit config image');
            return response([
                'message'=>'success',
                'data'=>null
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }

    }
}
