<?php

namespace App\Http\Controllers\comment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Comment;
use DB;

class CommentController extends Controller
{
    public function all_comment(Request $request)
    {
        $comment = Comment::all();
        return response([
            'message'=>'success',
            'data'=>$comment
        ]);
    }

    public static function getComment($subsubject_id = null)
    {
        try{
            if(! isset($subsubject_id)){
                return response([
                    'message'=>'Does not exist this sub_subject_id',
                    'data'=>[
                        'sub_subject_id'=>$request->sub_subject_id
                    ]
                ],400);
            }
            $comment = DB::select("SELECT u.name AS name,u.lastname AS lastname ,u.avatar AS avatar,u.id AS user_id,c.message AS message,c.created_at AS date_comment,c.sub_subject_id AS sub_subject_id,ss.title AS title FROM comment c,sub_subject ss,users u WHERE c.user_id = u.id AND c.sub_subject_id = ss.id AND ss.id = ?",[$subsubject_id]);
            if (! $comment){
                return response([
                    'message'=>'this sub_subject have not have comment yet',
                    'data'=>[]
                ],200);
            }else{
                return response([
                    'message'=>'success',
                    'data'=>$comment
                ],200);
            }
        } catch (\Exception $th){
            return response(['message'=>'service is not available','data'=>null],500);
        }
    }

}
