<?php

namespace App\Http\Controllers\comment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Comment;
use App\Banned_word;
use App\Comment_log;
use App\User;


class AddcommentController extends Controller
{
    private static function keep_log($user_id,$comment_id,$action){
        $log_comment = new Comment_log();
        $log_comment->user_id = $user_id;
        $log_comment->comment_id = $comment_id;
        $log_comment->action = $action;
        $log_comment->save();
    }
    public static function add(Request $request)
    {
        try{
            $access_validate = $request->bearerToken();
            try{
                $data = unserialize(base64_decode($access_validate));
            } catch (\Exception $th){
                return response([
                    'message'=>'Unauthorization',
                    'data'=>null
                ],401);
            }
            if($data['email'] == null){
                return response([
                    'message'=>'Unauthorization',
                    'data'=>[
                        'email'=>$data['email']
                    ]
                ],401);
            }
            $user_id = DB::select("SELECT id FROM users WHERE email = ? ",[$data['email']]);
            if(! $user_id){
                return response([
                    'message'=>'Unauthorization',
                    'data'=>[
                        'email'=>$data['email']
                    ]
                ],401);
            }

            if(! (isset($request->message)) &&
            (gettype($request->message == 'string'))){
                return response([
                    'message'=>'message does not exist',
                    'data'=>null
                ],400);
            }
            if(! (isset($request->sub_subject_id)) &&
            (gettype($request->message == 'integer'))){
                return response([
                    'message'=>'sub_subject_id does not exist',
                    'data'=>null
                ],400);
            }
            $sub_subject_id = DB::select("SELECT id FROM sub_subject WHERE id = ? LIMIT 1",[$request->sub_subject_id]);
            if(! $sub_subject_id){
                return response([
                    'message'=>'Does not exist this sub_subject_id',
                    'data'=>[
                        'sub_subject_id'=>$request->sub_subject_id
                    ]
                ],400);
            }
            $message = str_replace('fuck','***',$request->message);
            $banned_word = Banned_word::where('status','Active')->get();
            $banned = [];
            foreach($banned_word as $ban){
                array_push($banned,$ban->word);
            }
            $strwordArr =  $banned;
            $strCensor = "***" ;
            foreach ($strwordArr  as $value) {
                $message = str_replace($value,$strCensor ,$message);
            }
            $comment = new Comment();
            $comment->sub_subject_id = $request->sub_subject_id;
            $comment->user_id =$user_id[0]->id;
            $comment->message = $message;
            $comment->status= 'Active';
            $comment->save();
            AddcommentController::keep_log($user_id[0]->id,$comment->id,'Addcomment = "'.$request->message.'"');
            return response([
                'message'=>'Success',
                'data'=>[
                    'sub_subject_id'=>$request->sub_subject_id,
                    'user_id'=>$user_id[0]->id,
                    'message'=>$message,
                ]
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }

    }
    public function delete_comment(Request $request,$id)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        if(!isset($id)){
            return response(
                ['message'=>'error',
                'data'=>'"id" is require'
            ],400);
        }
        $validate = DB::select("SELECT * FROM comment WHERE status = 'Active' AND id = ?",[$id]);
        if(!$validate){
            return response(
                ['message'=>'error',
                'data'=>'this id does not exist'
            ],422);
        }
        DB::update("UPDATE comment SET status = 'Reject' WHERE id = ?",[$id]);
        $user = User::where('email',$data['email'])->get()->first()->id;
        AddcommentController::keep_log($user,$id,'Delete comment');
        return response(
            ['message'=>'success',
            'data'=>[
                'id'=>$id
            ]
        ],200);
    }
}
