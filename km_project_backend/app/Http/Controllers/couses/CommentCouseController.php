<?php

namespace App\Http\Controllers\couses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Courses_comment;
use App\Banned_word;
use App\User;
use App\Couse_comment_log;

class CommentCouseController extends Controller
{
    private static function keep_log($user_id,$action,$couse_comment_id)
    {
        $log_comment = new Couse_comment_log();
        $log_comment->user_id = $user_id;
        $log_comment->couse_comment_id = $couse_comment_id;
        $log_comment->action = $action;
        $log_comment->save();
        return $log_comment;
    }
    function formatDateThat($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear $strHour:$strMinute";
    }
    public function get_comment($id)
    {
        if(!(isset($id))){
            return response(
                ['message'=>'error',
                'data'=>'"id" of couse_vdo_id is require'
            ],400);
        }
        $data = DB::select(
            "SELECT cc.id,cc.course_vdo_id,cc.user_id,cc.comment,cc.status,cc.created_at,cc.updated_at,u.name,u.lastname,u.email,u.permission,u.avatar
            FROM courses_comments cc,users u,courses_vdo cv
            WHERE cv.id = cc.course_vdo_id AND cc.user_id = u.id AND cc.status = 'Active' AND cc.course_vdo_id = ?",[$id]);
        $res = [];
        foreach($data as $d){
            if(!$d->created_at == null){
                $d->created_at = $this->formatDateThat($d->created_at);
            }
            if(!$d->updated_at == null){
                $d->updated_at = $this->formatDateThat($d->updated_at);
            }
            array_push($res,$d);
        }
        if(!$res){
            return response([
                'message'=>'success',
                'data'=>'this couse video have not comment yet'
            ],200);
        }
        return response([
            'message'=>'success',
            'data'=>$res
        ],200);
    }
    public static function add_comment(Request $request)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        $request->validate([
            'comment'=>'required|string',
            'course_vdo_id'=> 'required'
        ]);
        $validate_course_vdo_id = DB::select(
            "SELECT id
            FROM courses_vdo
            WHERE status = 'active' AND id = ?",[$request->course_vdo_id]);
        if(!$validate_course_vdo_id){
            return response()->json([
                'message' => 'error',
                'data'=>'this couses vdo id does not exist'
            ],400);
        }
        $user_id = User::where('email',$data['email'])->get()->first();
        if(!$user_id){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        $message = $request->comment;
        $banned_word = Banned_word::where('status','Active')->get();
        $banned = [];
        foreach($banned_word as $ban){
            array_push($banned,$ban->word);
        }
        $strwordArr =  $banned;
        $strCensor = "***" ;
        foreach ($strwordArr  as $value) {
            $message = str_replace($value,$strCensor ,$message);
        }
        $user_id = $user_id->id;
        $comment = new Courses_comment();
        $comment->course_vdo_id = $request->course_vdo_id;
        $comment->user_id = $user_id;
        $comment->comment = $message;
        $comment->status = 'Active';
        $comment->save();
        $log_message = 'Add Comment = "'.$message.'"';
        CommentCouseController::keep_log($user_id,$log_message,$comment->id);
        return response([
            'message'=>'success',
            'data'=>$comment
        ],200);
    }
    public function delete_comment(Request $request, $id)
    {
        $access_validate = $request->bearerToken();
        try{
            $data = unserialize(base64_decode($access_validate));
        } catch (\Exception $th){
            return response([
                'message'=>'Unauthorization',
                'data'=>null
            ],401);
        }
        if(!$id){
            return response(
                ['message'=>'error',
                'data'=>'"id" is require'
            ],400);
        }
        $validate = DB::select("SELECT * FROM courses_comments WHERE status = 'Active' AND id = ?",[$id]);
        if(!$validate){
            return response(
                ['message'=>'error',
                'data'=>'this id does not exist'
            ],422);
        }
        DB::update("UPDATE courses_comments SET status = 'Reject' WHERE id = ?",[$id]);
        $user_id = User::where('email',$data['email'])->get()->first()->id;
        $log_message = 'Delete comment';
        CommentCouseController::keep_log($user_id,$log_message,$id);
        return response(
            ['message'=>'success',
            'data'=>[
                'id'=>$id
            ]
        ],200);
    }

}
