<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class ImageUploadController extends Controller
{
    public function imageUploadPost(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        $type_content_image = 'test_';
        $count_image_content = 1;
        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
        $request->image->move(public_path('img/myfolder'), $imageName);
        $file_path_to_database = '/img/myfolder/'.$imageName;

    }

    public function uploadImage(Request $request,$i,$path_pic)
    {
        $type_content_image = $path_pic.'_';
        $count_image_content = $i;
        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
        $request->image->move(public_path('img/'.$path_pic), $imageName);
        $file_path_to_database = '/img/'.$path_pic.'/'.$imageName;

        // return $file_path_to_database;
        return response(['message'=>'success','data'=>$file_path_to_database],200);

    }

}
