<?php

namespace App\Http\Controllers\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\content_backgroud;
use App\Content_type;
use DB;
use App\Content_background_log;
use App\User;

class ContenBackgroudController extends Controller
{
    private static function keep_log($user_id,$content_bg_id,$action){
        $log = new Content_background_log();
        $log->user_id = $user_id;
        $log->content_background_id = $content_bg_id;
        $log->action = $action;
        $log->save();
    }
    public function index()
    {
        $content = content_backgroud::where('status','active')->get();
        return response([
            'message'=>'success',
            'data'=>$content
        ],200);
    }
    public function add(Request $request)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        $request->validate([
            'type_id'=>'required',

            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048'
        ]);
        $content_id_validate = DB::select("SELECT * FROM content_type WHERE id = ?",[$request->type_id]);
        if(!$content_id_validate){
            return response([
                'message'=>'error',
                'data'=>'does not exist this content type'
            ],422);
        }
        else{
            $content_bg = new content_backgroud();
            $content_bg->type_id = $request->type_id;
            $type_content_image = 'contentBackGround_';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
            $request->image->move(public_path('img/content_background'), $imageName);
            $file_path_to_database = '/img/content_background/'.$imageName;
            $content_bg->pic_path = $file_path_to_database;
            $content_bg->status = 'active';
            $content_bg->save();
            $user_id = User::where('email',$data['email'])->get()->first();
            $log = ContenBackgroudController::keep_log($user_id->id,$content_bg->id,'Add backgound');
            return response([
                'message'=>'success',
                'data'=>[
                    'type_id'=>$request->type_id
                ]
            ],201);
        }

    }
    public function edit(Request $request)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        $request->validate([
            'bg_id'=>'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048'
        ]);
        $content_id_validate = DB::select("SELECT * FROM content_background WHERE id = ?",[$request->bg_id]);
        if(!$content_id_validate){
            return response([
                'message'=>'error',
                'data'=>'does not exist this content background'
            ],422);
        }
        else{
            $type_content_image = 'contentBackGround_';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
            $request->image->move(public_path('img/content_background'), $imageName);
            $file_path_to_database = '/img/content_background/'.$imageName;
            DB::update("UPDATE content_background SET pic_path = ? WHERE id = ?",[$file_path_to_database,$request->bg_id]);
            $user_id = User::where('email',$data['email'])->get()->first();
            $log = ContenBackgroudController::keep_log($user_id->id,$request->bg_id,'edit backgound');
            return response([
                'message'=>'success',
                'data'=>[
                    'bg_id'=>$request->bg_id
                    ]
                ],200);
        }
    }
    public function delete(Request $request,$id=null)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        if($id == null){
            return response([
                'message'=>'error',
                'data'=>'need id'
            ],400);
        }else{
            if(!DB::select("SELECT * FROM content_background WHERE id = ?", [$id])){
                return response([
                    'message'=>'error',
                    'data'=>'does not exist this background'
                ],400);
            }
            $content_bg = content_backgroud::find($id);
            $content_bg->status = 'reject';
            $content_bg->save();
            $user_id = User::where('email',$data['email'])->get()->first();
            $log = ContenBackgroudController::keep_log($user_id->id,$id,'edit backgound');

            return response([
                'message'=>'success',
                'data'=>[
                    'id'=>$id
                ]
            ],200);
        }

    }
    public function statusbg(Request $request, $id)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        $request->validate([
            'approve' => 'required',
        ]);
        $news = content_backgroud::where('id',$id)->get()->first();
        if (!$news){
            return response([
                'message'=>'error',
                'data'=>'Can not find this ID'
            ],422);
        }
        if($request->approve == 'Allowed') {
            $news->status = 'active';
            $news->save();
            $user_id = User::where('email',$data['email'])->get()->first();
            $log = ContenBackgroudController::keep_log($user_id->id,$id,'Active backgound');
            return response([
                'message'=>'Approved',
                'data'=>$news
            ],200);
        }
        else{
            $news->status = 'reject';
            $news->save();
            $user_id = User::where('email',$data['email'])->get()->first();
            $log = ContenBackgroudController::keep_log($user_id->id,$id,'Reject backgound');
            return response([
                'message'=>'Not approved',
                'data'=>$news
            ],200);
        }
    }
    public function index_by_type($id)
    {
        $content_bg = DB::select("SELECT * FROM content_background WHERE status = 'active' AND type_id = ?",[$id]);
        if(!$content_bg){
            return response([
                'message'=>'success',
                'data'=>'does not exist background'
            ],200);
        }
        return response([
            'message'=>'success',
            'data'=>$content_bg
        ],200);
    }
}
