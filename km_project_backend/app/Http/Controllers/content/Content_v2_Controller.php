<?php

namespace App\Http\Controllers\content;

use App\Content_type;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class Content_v2_Controller extends Controller
{
    public function index()
    {
        // try{
            $content_type = DB::select('SELECT distinct id,name FROM content_type WHERE status = "active" OR status = "Active" ORDER BY id DESC');
            $content_result_last = [];
            $last = [];
            foreach($content_type as $type){
                $content_result_last['type_name'] = $type->name;
                $content_array = [];
                $content = DB::select("SELECT distinct * FROM content WHERE type_id = ?  AND (status = 'Active' OR status = 'active') ORDER BY id DESC",[$type->id]);
                foreach($content as $con){
                    $picture = [];
                    $pic = DB::select('SELECT distinct cp.path_pic AS pic,cp.id AS id
                    FROM content_type ct,content c,content_picture cp
                    WHERE c.type_id = ct.id AND c.id = cp.content_id AND (c.status = "Active" OR c.status = "active") AND (ct.status = "Active" OR ct.status = "active") AND (cp.status = "active" OR cp.status = "Active") AND c.id = ? ORDER BY id DESC',[$con->id]);
                    foreach($pic as $p){
                        array_push($picture,$p->pic);

                    }
                    if($con->created_at!=null){
                        $con->created_at = $this->formatDateThat($con->created_at);
                    }
                    if($con->updated_at!=null){
                        $con->updated_at = $this->formatDateThat($con->updated_at);
                    }
                    $con->picture = $picture;
                }

                array_push($content_array,$content);

                $content_result_last['data'] = $content_array[0];
                array_push($last,$content_result_last);
            }

            return response([
                'message'=>'success',
                'data'=>$last
            ],200);
        // } catch (\Exception $th){
        //     return response([
        //         'message'=>'error',
        //         'data'=>'service is not available'
        //     ],500);
        // }
    }

    public function index_by_id($id)
    {
        // try{
            $content_type = DB::select('SELECT distinct id,name FROM content_type WHERE (status = "Active" OR status = "active") AND id = ? ORDER BY id DESC',[$id]);
            $content_result_last = [];
            $last = [];
            foreach($content_type as $type){
                $content_result_last['type_name'] = $type->name;
                $content_array = [];
                $content = DB::select("SELECT distinct * FROM content WHERE type_id = ? AND (status = 'Active' OR status = 'active') ORDER BY id DESC",[$type->id]);
                foreach($content as $con){
                    $picture = [];
                    $pic = DB::select('SELECT distinct cp.path_pic AS pic,cp.id AS id
                    FROM content_type ct,content c,content_picture cp
                    WHERE c.type_id = ct.id AND c.id = cp.content_id AND (c.status = "Active" OR c.status = "active") AND (ct.status = "Active" OR ct.status = "active") AND (cp.status = "active" OR cp.status = "Active") AND c.id = ? ORDER BY id DESC',[$con->id]);
                    foreach($pic as $p){
                        array_push($picture,$p->pic);

                    }
                    if($con->created_at!=null){
                        $con->created_at = $this->formatDateThat($con->created_at);
                    }
                    if($con->updated_at!=null){
                        $con->updated_at = $this->formatDateThat($con->updated_at);
                    }
                    $con->picture = $picture;
                }

                array_push($content_array,$content);

                $content_result_last['data'] = $content_array[0];
                array_push($last,$content_result_last);
            }

            return response([
                'message'=>'success',
                'data'=>$last
            ],200);
        // } catch (\Exception $th){
        //     return response([
        //         'message'=>'error',
        //         'data'=>'service is not available'
        //     ],500);
        // }
    }
    static function formatDateThat($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear $strHour:$strMinute";
    }
      public function content_type()
    {
        // $content_type = Content_type::select('SELECT distinct id,name FROM content_type WHERE (status = "Active" OR status = "active") AND id = ? ORDER BY id DESC',[$id]);
        $content_type = Content_type::where('status',"active","name")->get()->all();
        // dd($content_type);
        return response(['data'=>$content_type],200);



}
}

