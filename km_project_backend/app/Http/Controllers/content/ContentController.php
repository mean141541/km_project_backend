<?php

namespace App\Http\Controllers\content;

use App\Http\Controllers\content\ContenBackgroudController;
use App\Content_type;
use App\Content;
use App\User;
use App\Content_log;
use App\Content_picture;
use App\Content_picture_log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class ContentController extends Controller
{
    private static function keep_log($user_id,$content_id,$action){
        $log = new Content_log();
        $log->user_id = $user_id;
        $log->content_id = $content_id;
        $log->action = $action;
        $log->save();
    }
    private static function keeppic_log($user_id,$detail,$action,$id){
        $log = new Content_picture_log();
        $log->user_id = $user_id;
        $log->picture_id = $id;
        $log->action = $action;
        $log->save();
    }
    public function index()
    {

        $content = DB::select('SELECT c.id AS id,ct.name AS content,c.title AS title,c.detail AS detail
        FROM content_type ct,content c
        WHERE c.type_id = ct.id AND c.status = "Active" AND ct.status = "Active"');
        $content_result = [];
        foreach($content as $con){
            $pic = DB::select('SELECT cp.content_id, cp.path_pic AS pic
            FROM content_type ct,content c,content_picture cp
            WHERE c.type_id = ct.id AND c.id = cp.content_id AND c.status = "Active" AND ct.status = "Active"');
            $con->picture = [];
            foreach($pic as $p){
                if($con->id == $p->content_id){
                    array_push($con->picture,$p->pic);
                }
            }
            array_push($content_result,$con);

        }

        return response(['message'=>'success','data'=>$content_result],200);
    }

    public function indexAll()
    {

        $content = DB::select('SELECT c.id AS id,ct.name AS content,c.title AS title,c.detail AS detail ,c.status AS status ,ct.name AS name
        FROM content_type ct,content c
        WHERE c.type_id = ct.id');
        $content_result = [];
        foreach($content as $con){
            $pic = DB::select('SELECT cp.content_id, cp.path_pic AS pic
            FROM content_type ct,content c,content_picture cp
            WHERE c.type_id = ct.id AND c.id = cp.content_id');
            $con->picture = [];
            foreach($pic as $p){
                if($con->id == $p->content_id){
                    array_push($con->picture,$p->pic);
                }
            }
            array_push($content_result,$con);

        }
        return response(['message'=>'success','data'=>$content_result],200);
    }

    public function index2()
    {
        // return 'test';
        $news = DB::select('SELECT c.id AS id,ct.name AS content,c.title AS title,c.detail AS detail
        FROM content_type ct,content c
        WHERE c.type_id = ct.id AND c.status = "Active" AND ct.status = "Active"');
        // return $news;
        $news_result = [];
        foreach($news as $newsdata){
            $newspicture = DB::select('SELECT np.news_id, np.path_pic AS pathpic
            FROM news_type nt, news n, news_picture np
            WHERE nt.id = n.type_id AND n.id = np.news_id AND n.status = "active" AND np.status = "active"');

            $newsdata->picture =[];
            foreach($newspicture as $pic){
                if($newsdata->id == $pic->news_id){
                    array_push($newsdata->picture,$pic->pathpic);
                }
            }
            array_push($news_result,$newsdata);
        }
        if($news_result == null){
            return response(['message'=>'Activity not found']);
        }
        else{
            return response(['message'=>'success','data'=>$news_result],200);
        }
    }
    // public function travel($id)
    // {
    //     $content = DB::select('SELECT c.id AS id,ct.name AS content,c.title AS title,c.detail AS detail
    //     FROM content_type ct,content c
    //     WHERE c.type_id = ct.id AND c.status = "Active" AND ct.status = "Active"');
    //     $content_result = [];
    //     foreach($content as $con){
    //         $pic = DB::select('SELECT cp.content_id, cp.path_pic AS pic
    //         FROM content_type ct,content c,content_picture cp
    //         WHERE c.type_id = ct.id AND c.id = cp.content_id AND c.status = "Active" AND ct.status = "Active"');
    //         $con->picture = [];
    //         foreach($pic as $p){
    //             if($con->id == $p->content_id){
    //                 array_push($con->picture,$p->pic);
    //             }
    //         }
    //         array_push($content_result,$con);
    //     }
    //     // $content = DB::select('SELECT c.id AS id,ct.name AS content,c.title AS title,c.detail AS detail
    //     // FROM content_type ct,content c,content_picture cp
    //     // WHERE c.type_id = ct.id AND c.id = cp.content_id AND c.status = "Active" AND ct.status = "Active"');


    //     return response(['message'=>'success','data'=>$content_result],200);
    // }
    public function clickedit($id)
    {
        $content = Content::where('id',$id)->get()->first();
        // dd($can);
        // dd($content);
        // $content2 = Content_picture::all();
        return response(['message'=>'success','data'=>$content],200);

    }
    public function updateedit(Request $request,$id = null)
        {
            // return $request;
        // $request = DB::table('content')
        //     ->where('id', $id)
        //     ->update(['title' => $request->title,
        //     'detail' => $request->detail,
        //     'type_id' => $request->type_id]);
            // dd($request);
        $content = Content::where('id',$id)->get()->first();
        $content->title = $request->title;
        $content->detail = $request->detail;
        $content->type_id= $request->type_id;
        $content->type_id= $request->type_id;
        $content->status= $request->status;
        // dd($content);
        $content->save();
        // dd($content);
            //  return response(['message'=>'success','data'=>$request],200);
            return redirect()->route('Travel.index');
        $action = 'edit';
        $news = Content::where('id',$id)->get()->first();
        $cont =  Content_type::where('id',$request->type_id)->get()->first();
        if(! $cont){
            return "type_id does not Database";
        }
        if (!$news){
            return response(['message'=>'Success', 'data'=>'This ID not found'],422);
        }
        if(! (isset($request->type_id)) &&
            (gettype($request->type_id == 'int'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Type_id does not exist'
                ],400);
        }
        if(! (isset($request->title)) &&
            (gettype($request->title == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Title does not exist'
                ],400);
        }
        if(! (isset($request->detail)) &&
            (gettype($request->detail == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Detail does not exist'
                ],400);
        }

        $access_validate = $request->bearerToken();
        //return $access_validate;
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                // return $request;
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    if($news->status == 'active'){
                        $news->title = $request->title;
                         $news->detail = $request->detail;
                         $news->type_id= $request->type_id;
                        $news->save();
                        $user_id = User::where('email',$data['email'])->get()->first();
                        ContentController::keep_log($user_id->id, $id,$action);
                        return response(['message'=>'Success', 'data'=>$news],200);
                    } else{
                        return response(['message'=>'Unsuccess', 'data'=>'You can not fix, News status: not active',422]);
                    }
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader']);
                }
            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }
            // return $request;
        // $request = DB::table('content')
        //     ->where('id', $id)
        //     ->update(['title' => $request->title,
        //     'detail' => $request->detail,
        //     'type_id' => $request->type_id]);
            // dd($request);
        $content = Content::where('id',$id)->get()->first();
        $content->title = $request->title;
        $content->detail = $request->detail;
        $content->type_id= $request->type_id;
        $content->status= $request->status;
        // dd($content);
        // $content->save();
        // dd($content);
            //  return response(['message'=>'success','data'=>$request],200);
            // $user_id = User::where('email',$data['email'])->get()->first();
            // NewsController::keeplog($user_id->id, $action, '"'.$news->title.'","'.$news->detail.'"', $news->id);
            // return redirect()->route('Travel.index');
    }
    public function statusedit(Request $request,$id = null)

        {
            if($request->status != 'Active' && $request->status != 'reject'){

                return response(['message'=>'Status Invalid'],400);

            }
            else
            { $request = DB::table('content')
                ->where('id', $id)
                ->update(['status' => $request->status

               ]);
               return response(['message'=>'success','data'=>$request],200);
            }
    }

    public function inserts( Request $request)
    {
        $action = 'create';
        $cont =  Content_type::where('id',$request->type_id)->get()->first();
        if(! $cont){
            return "type_id does not Database";
        }
        if(! (isset($request->type_id)) &&
            (gettype($request->type_id == 'int'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'type_id does not exist'
                ],400);
        }
        if(! (isset($request->title)) &&
            (gettype($request->title == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Title does not exist'
                ],400);
        }
        if(! (isset($request->detail)) &&
            (gettype($request->datail == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Detail does not exist'
                ],400);
        }

        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));

                // return $data;
                $news = new Content();
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $news->type_id = $request->type_id;
                    $news->title = $request->title;
                    $news->detail = $request->detail;
                    $news->created_at = date('Y-m-d H:i:s');
                    $news->updated_at = date('Y-m-d H:i:s');
                    $news->status = "active";
                    $news->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    ContentController::keep_log($user_id->id, $news->id,$action);
                    return response(['message'=>'Success', 'data'=>$news],200);

                }else{
                    return response(['message'=>'Unsuccess', 'data'=>'You are not Admin or Leader']);
                }
            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }

        //  dd($request);
        $data->validate([
        'title'=>'required',
        'detail'=>'required',
        'type_id'=>'required',
        // 'status'=>'required',
        // 'created_at'=>'required',
        // 'updated_at'=>'required',


        ]);
        $cont = new Content();
        $cont->title =$data['title'];
        $cont->detail =$data['detail'];
        $cont->type_id =$data['type_id'];
        $cont->created_at = date('Y-m-d H:i:s');
        $cont->updated_at = date('Y-m-d H:i:s');
        $cont->status = 'waiting';
        $cont->save();

        // return redirect()->back();
        return response(['message'=>'success', 'data'=>$cont],200);


    }
    public function delete(Request $request, $id){

        $action = 'delete';
        $news = Content::where('id',$id)->get()->first();
        if (!$news){
            return response(['message'=>'Dont ID', 'data'=>'This ID not found'],422);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $news->status = 'reject';
                    $news->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    ContentController::keep_log($user_id->id, $news->id,$action);
                    return response(['message'=>'Success', 'data'=>$news],200);
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader']);
                }
            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }
        $news = Content::where('id',$id)->get()->first();
        if (!$news){
            return response(['message'=>'Can not find this ID'],422);
        }
        $news->status = 'reject';
        $news->save();
        return response(['message'=>'success', 'data'=>$news],200);

    }

    /////////////////// รูปภาพ//////////////////////////////////


    public function imageUpload(Request $request ,$id)
    {

        $action = 'upload';
        if(! (isset($request->image)) &&
            (gettype($request->image == 'image|mimes:jpeg,png,jpg,svg|max:2048'))){
                return response([
                    'message'=>'Image does not exist',
                    'data'=>null
                ],400);
        }
        if(! (isset($request->type_imageName)) &&
            (gettype($request->type_imageName == 'string|max:30'))){
                return response([
                    'message'=>'type_imageName does not exist',
                    'data'=>null
                ],400);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                $news_type_image = $request->type_imageName;
                $count_image = 1;
                $imageName = $news_type_image.'_'.date('YmdHis').'_'.$count_image.'.'.$request->image->extension();
                $request->image->move(public_path('img/content'), $imageName);
                $file_path_to_database = '/img/content/'.$imageName;

                $imagePath = new Content_picture();
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $imagePath->content_id = $id;
                    $imagePath->path_pic = $file_path_to_database;
                    $imagePath->status = "active";
                    $imagePath->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    ContentController::keeppic_log($user_id->id, $action, $imagePath->path_pic, $imagePath->id);
                    return response(['message'=>'Success','data'=>$imagePath],200);
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader'],200);
                }

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }
    }
    public function addstatusImage(Request $request, $id)
    {

        // return $request->approve;
        if(!(isset($request->approve))&&
        (gettype($request->approve == 'string'))){
            return response(['message'=>'No data', 'data'=>null], 400);
            }
        $news = content_picture::where('id',$id)->get()->first();
        if (!$news){
        return response(['message'=>'Can not find this ID'],422);
        }
        if($request->approve == 'Allowed') {
            $news->status = 'active';
            $news->save();
            return response(['message'=>'Approved','data'=>$news],200);
        }
        else{
            $news->status = 'reject';
            $news->save();
            return response(['message'=>'Not approved', 'data'=>$news],200);
        }
    }

    public function imageDelete($id)
    {
        $image = content_picture::where('id',$id)->get()->first();
        if (!$image){
            return response(['message'=>'Can not find this ID'],422);
        }
        $image->status = 'reject';
        $image->save();
        return response(['message'=>'success', 'data'=>$image],200);
        // $image = Content_picture::where('id',$id);
        // DB::delete('DELETE FROM `content_picture` WHERE id  = ?', [$id]);
        // $image->save();
        // return response(['message'=>'success'],200);
    }

    public function imageEdit(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'type_imageName' => 'required',
        ]);
        $news_type_image = $request->type_imageName;
        $count_image = 1;
        $imageName = $news_type_image.'_'.date('YmdHis').'_'.$count_image.'.'.$request->image->extension();
        $request->image->move(public_path('img/content'), $imageName);
        $file_path_to_database = '/img/content/'.$imageName;
        DB::update('UPDATE content_picture SET path_pic = ? WHERE content_picture.id = ?',[$file_path_to_database, $id] );
        return response(['message'=>'success'],200);
    }


    //////////////////////////content_type/////////////////////////////////////
    public function insertstype( Request $data)
    {

        //  dd($request);
        $data->validate([
        'name'=>'required'


        ]);
        $typeimg = new ContenBackgroudController;
        // $typeimg->add($data);
        $cont = new Content_type();
        $cont->name =$data['name'];
        // $cont->detail =$data['detail'];
        // $cont->type_id =$data['type_id'];
        $cont->created_at = date('Y-m-d H:i:s');
        $cont->updated_at = date('Y-m-d H:i:s');
        $cont->status = 'active';
        $cont->save();

            $type_id=Content_type::where('name',$data['name'])->get()->first()->id;

            $data['type_id']=$type_id;
            // $data->status = 'active';
         $typeimg->add($data);


        // return redirect()->back();
        return response(['message'=>'success', 'data'=>$cont],200);


    }
    public function deletetype($id){

        // $typeimg = new ContenBackgroudController;
        $news = Content_type::where('id',$id)->get()->first();
        $news->status = 'reject';
        $news->save();

        return response(['message'=>'success', 'data'=>$news],200);
        }

        public function statusedittype(Request $request,$id = null)

        {
            if($request->status != 'active' && $request->status != 'reject'){

                return response(['message'=>'Status Invalid'],400);

            }
            else
            { $request = DB::table('content_type')
                ->where('id', $id)
                ->update(['status' => $request->status

               ]);
               return response(['message'=>'success', 'data'=>$request],200);
            }
        }
        public function edittype(Request $request,$id = null)
        {
            // return $request;
        $request = DB::table('content_type')
            ->where('id', $id)
            ->update(['name' => $request->name
            ]);
            // dd($request);
             return response(['message'=>'success','data'=>$request],200);
    }

    public function saveImg($img_path,$news_id)
    {
        $img = new Content_picture();
        $img->content_id = $news_id;
        $img->path_pic = $img_path;
        $img->status = 'active';
        $img->save();
    }
}
