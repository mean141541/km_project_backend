<?php

namespace App\Http\Controllers\course_vdo;

use App\Course_vdo_log;
use App\Courses_vdo;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class CourseVdoController extends Controller
{
    private static function save_log($user_id, $action, $course_vdo_id)
    {
        $log_vdo = new Course_vdo_log();
        $log_vdo->user_id = $user_id;
        $log_vdo->course_vdo_id = $course_vdo_id;
        $log_vdo->action = $action;
        $log_vdo->save();
        return $log_vdo;
    }
    function formatDateThat($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));
        $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear $strHour:$strMinute";
    }
    public function index()
    {
        $courses_vdo = Courses_vdo::all();
        $res = [];
        foreach ($courses_vdo as $cd) {
            if (!$cd->created_at == null) {
                $cd->created_at_thai = $this->formatDateThat($cd->created_at);
            }
            if (!$cd->updated_at == null) {
                $cd->updated_at_thai = $this->formatDateThat($cd->updated_at);
            }
            array_push($res, $cd);
        }
        return response(['message' => 'Show Courses Vdo All Success', 'data' => $res], 200);
    }
    public function create(Request $request)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));
        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data' => $access_validate
            ], 401);
        }
        $request->validate(
            [
                'title' => 'required|string|max:100',
                'detail' => 'required|string',
                'path_vdo' => 'required|string'
            ]
        );
        if (
            !(isset($request->title)) &&
            (gettype($request->title == 'string'))
        ) {
            return response([
                'message' => 'Title does not exist',
                'data' => null
            ], 400);
        }
        if (
            !(isset($request->detail)) &&
            (gettype($request->detail == 'string'))
        ) {
            return response([
                'message' => 'Detail does not exist',
                'data' => null
            ], 400);
        }
        if (
            !(isset($request->path_vdo)) &&
            (gettype($request->path_vdo == 'string'))
        ) {
            return response([
                'message' => 'Vdo does not exist',
                'data' => null
            ], 400);
        }
        $user_id = User::where('email', $data['email'])->get()->first();
        if (!$user_id) {
            return response()->json([
                'message' => 'Unauthorization',
                'data' => $access_validate
            ], 401);
        }
        $user_id = $user_id->id;
        $courses_vdo = new Courses_vdo();
        $courses_vdo->course_id = $request->course_id;
        $courses_vdo->title = $request->title;
        $courses_vdo->detail = $request->detail;
        $courses_vdo->path_vdo = $request->path_vdo;
        $courses_vdo->status = $request->status;
        $courses_vdo->view = $request->view;
        $courses_vdo->save();
        $log_message = 'Add = ' . $request->input('title') . '';
        CourseVdoController::save_log($user_id, $log_message, $courses_vdo->id);
        return response(['message' => 'Create success', 'data' => $courses_vdo], 200);
    }

    public function getId($id)
    {
        $courses_vdo = Courses_vdo::Where('id', $id)->get()->first();
        if ($courses_vdo == null) {
            return response(['message' => 'Courses_vdo ID not fund']);
        } else {
            return response(['message' => 'Success', 'data' => $courses_vdo], 200);
        }
    }
    public function show($course_id)
    {
        $courses_vdo = Courses_vdo::Where('course_id', $course_id)->get();
        // $courses_vdo = Courses_vdo::Where('course_id', $course_id)->orderBy('id', 'desc')->get();
        // dd($courses_vdo);
        $res = [];
        foreach ($courses_vdo as $cd) {
            if (!$cd->created_at == null) {
                $cd->created_at_thai = $this->formatDateThat($cd->created_at);
            }
            if (!$cd->updated_at == null) {
                $cd->updated_at_thai = $this->formatDateThat($cd->updated_at);
            }
            array_push($res, $cd);
        }
        return response(['message' => 'Show Courses Vdo All Success', 'data' => $res], 200);


    }

    public function update(Request $request, $id)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));
        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data' => $access_validate
            ], 401);
        }
        $request->validate(
            [
                'title' => 'required|string|max:100',
                'detail' => 'required|string',
                'path_vdo' => 'required|string'
            ]
        );
        if (
            !(isset($request->title)) &&
            (gettype($request->title == 'string'))
        ) {
            return response([
                'message' => 'Title does not exist',
                'data' => null
            ], 400);
        }
        if (
            !(isset($request->detail)) &&
            (gettype($request->detail == 'string'))
        ) {
            return response([
                'message' => 'Detail does not exist',
                'data' => null
            ], 400);
        }
        if (
            !(isset($request->path_vdo)) &&
            (gettype($request->path_vdo == 'string'))
        ) {
            return response([
                'message' => 'Vdo does not exist',
                'data' => null
            ], 400);
        }
        $user_id = User::where('email', $data['email'])->get()->first();
        if (!$user_id) {
            return response()->json([
                'message' => 'Unauthorization',
                'data' => $access_validate
            ], 401);
        }
        $user_id = $user_id->id;
        // $courses_vdo = Courses_vdo::where('id',$id)->get()->first();
        $courses_vdo = Courses_vdo::find($id);
        $courses_vdo->course_id = $request->course_id;
        $courses_vdo->title = $request->title;
        $courses_vdo->detail = $request->detail;
        $courses_vdo->path_vdo = $request->path_vdo;
        $courses_vdo->status = $request->status;
        $courses_vdo->view = $request->view;


        // if ($data['permission'] == 'Admin') {
        //     $courses_vdo->status = "active";
        // }
        // if ($data['permission'] == 'User') {
        //     $courses_vdo->status = "";
        // }
        // return $data;
        // return  $user_id;
        $courses_vdo->save();
        $log_message = 'Update Id = ' . $id . ' ,Title = ' . $request->input('title') . '';
        CourseVdoController::save_log($user_id, $log_message, $courses_vdo->id);
        return response(['message' => 'Update success', 'data' => $courses_vdo], 200);
    }

    public function delete(Request $request, $id)
    {
        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));
        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data' => $access_validate
            ], 401);
        }
        $user_id = User::where('email', $data['email'])->get()->first();
        if (!$user_id) {
            return response()->json([
                'message' => 'Unauthorization',
                'data' => $access_validate
            ], 401);
        }
        $user_id = $user_id->id;
        $courses_vdo = Courses_vdo::find($id);
        if (!$courses_vdo) {
            return response(['message' => 'Can not find this ID'], 422);
        }
        $courses_vdo->status = 'reject';
        $courses_vdo->save();
        $log_message = 'Delete Id = ' . $id . ' ,Title = ' . $courses_vdo->title . '';
        CourseVdoController::save_log($user_id, $log_message, $courses_vdo->id);
        return response(['message' => 'Delete Courses Vdo Success', 'data' => $courses_vdo], 200);
    }
}
