<?php

namespace App\Http\Controllers\Department;

use App\Department;
use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Http\Request;
use DB;

class DepartmentController extends Controller
{

//ส่วนหน้า Index
    public function index()
    {
        // $department = Department::all();
        // return response(['message'=>'success','data'=>$department],200);
        $department = DB::select('SELECT id,department_name,pic,created_at,updated_at,status
        FROM department
        WHERE id AND status = "active"');
        return response(['message'=>'success','data'=>$department],200);
    }

    public function depart($id=null)
    {

        $department = Department::where('id',$id)->get()->first();
        return response(['message'=>'success','data'=>$department],200);

    }


    public function create(Request $request)
    {

        $data = request()->validate(
                [
                // 'approve'=>'required',
                'department_name'=>'required|string|max:30',
                'pic'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                ]
        );
        if(! (isset($request->department_name)) &&
        (gettype($request->department_name == 'string'))){
            return response([
                'message'=>'Title does not exist',
                'data'=>null
            ],400);
        }
        $access_validate = $request->bearerToken();
            if (! $access_validate){
            return response()->json([
            'message' => 'Unauthorization',
            'data'=>$access_validate
        ],401);
        }else{

        try {

        $data = unserialize(base64_decode($access_validate));
        $department = new Department();

        if($request->pic != null){
            $type_content_image = 'department_pic';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->pic->extension();
            $request->pic->move(public_path('img/department'), $imageName);
            $file_path_to_database = '/img/department/'.$imageName;
            $department->pic = $file_path_to_database;
        }else{
            $department->pic = '';
        }
            $department->department_name = $request->input('department_name');
            $department->created_at = date('Y-m-d H:i:s');
            $department->updated_at = date('Y-m-d H:i:s');

        if ($data['permission'] == 'Admin'){
            $department->status = "active";
        }
        if ($data['permission'] == 'Leader'){
            $department->status = "active";
        }
        } catch (\Exception $th) {
            return response()->json([
            'message' => 'Unauthorization',
            'date'=>$th
        ],401);
        }
        }
        $department->save();

        return response(['message'=>'success','data'=>$department],200);
    }


    public function update(request $request, $id=null){

        $data = request()->validate(
                [
                // 'approve'=>'required',
                'department_name'=>'required|string|max:30',
                'pic'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                ]
        );
    if(! (isset($request->department_name)) &&
    (gettype($request->department_name == 'string'))){
        return response([
            'message'=>'Title does not exist',
            'data'=>null
        ],400);
        }

        $access_validate = $request->bearerToken();
            if (! $access_validate){
            return response()->json([
            'message' => 'Unauthorization',
            'data'=>$access_validate
        ],401);
        }else{

        try {

        $data = unserialize(base64_decode($access_validate));
        $department = Department::find($id);

        if($request->pic != null){
            $type_content_image = 'department_pic';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->pic->extension();
            $request->pic->move(public_path('img/department'), $imageName);
            $file_path_to_database = '/img/department/'.$imageName;
            $department->pic = $file_path_to_database;
        }else{
            $department->pic = '';
        }
            $department->department_name = $request->input('department_name');
            $department->created_at = date('Y-m-d H:i:s');
            $department->updated_at = date('Y-m-d H:i:s');
            // $department->status = $request->input('status');
            $department->status = "";
        if ($data['permission'] == 'Admin'){
            $department->status = "active";
        }
        if ($data['permission'] == 'Leader'){
            $department->status = "active";
        }
        } catch (\Exception $th) {
            return response()->json([
            'message' => 'Unauthorization',
            'date'=>$th
        ],401);
        }
        }
        $department->save();

        return response(['message'=>'success','data'=>$department],200);
    }


        public function deletedepartment($id=null){

            $department = Department::where('id',$id)->get()->first();
            if (!$department){
                return response(['message'=>'Can not find this ID'],422);
            }
            $department->status = 'reject';
            $department->save();
            return response(['message'=>'success', 'data'=>$department],200);
        }

    //ส่วนหน้า Index
    //-----------------------------------------//


    //ส่วนหน้า subjectใช้หน้า Admin

    public function subject()
        {
        // $subject = Subject::all();
        // return response(['message'=>'success','data'=>$subject],200);
        $subject = DB::select('SELECT id,department_id,name,pic_path,created_at,updated_at,status
        FROM subject
        WHERE id AND status = "active"');
        return response(['message'=>'success','data'=>$subject],200);
        }

    public function subjectid($id=null)
        {
        $subject = Subject::where('id',$id)->get()->first();
        return response(['message'=>'success','data'=>$subject],200);
        }


}
