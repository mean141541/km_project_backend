<?php

namespace App\Http\Controllers\news;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\content\Content_v2_Controller;
use App\News_type;
use App\News_type_log;
use App\User;
use DB;

class NewstypeController extends Controller
{
    public function keeplog($user_id, $action, $detail, $type_id)
    {
        $log = new News_type_log;
        $log->user_id = $user_id;
        $log->action = $action;
        $log->detail = $detail;
        $log->type_id = $type_id;
        $log->save();

    }

    public function insertType(Request $request)
    {
        $action = 'create';
        if(! (isset($request->type_name)) &&
            (gettype($request->type_name == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Type_name does not exist'
                ],400);
        }
        $access_validate = $request->bearerToken();
        $data = unserialize(base64_decode($access_validate));
        $news = new News_type();
        $news->type_name = $request->type_name;
        $news->status = 'active';
        $news->save();
        $user_id = User::where('email',$data['email'])->get()->first();
        NewstypeController::keeplog($user_id->id, $action, $news->type_name, $news->id);
        return response(['message'=>'Success', 'data'=>$news],200);
    }

    public function editType(Request $request , $id)
    {
        $action = 'edit';
        if(! (isset($request->type_name)) &&
            (gettype($request->type_name == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Type_name does not exist'
                ],400);
        }
        $access_validate = $request->bearerToken();
        $data = unserialize(base64_decode($access_validate));
        $news_old = News_type::where('id',$id)->get()->first();
        $news = News_type::where('id',$id)->get()->first();
        if (!$news){
            return response(['message'=>'Success', 'data'=>'This ID not found'],422);
        }
        $news->type_name = $request->type_name;
        $news->status = 'active';
        $news->save();
        $user_id = User::where('email',$data['email'])->get()->first();
        NewstypeController::keeplog($user_id->id, $action, 'Change: "'.$news_old->type_name.'" To: "'.$news->type_name.'"', $news->id);
        return response(['message'=>'Success','data'=>$news],200);
    }

    public function deleteType(Request $request,$id)
    {
        $action = 'delete';
        $news = News_type::where('id',$id)->get()->first();
        if (!$news){
            return response(['message'=>'Success', 'data'=>'This ID not found'],422);
        }
        $access_validate = $request->bearerToken();
        $data = unserialize(base64_decode($access_validate));
        $news->status = 'reject';
        $news->save();
        $user_id = User::where('email',$data['email'])->get()->first();
        NewstypeController::keeplog($user_id->id, $action, $news->type_name, $news->id);
        return response(['message'=>'Success', 'data'=>$news],200);
    }

    public function indexType()
    {
        $contr = new Content_v2_Controller;
        //$typenews = News_type::all();
        $typenews = DB::select('SELECT *
        FROM news_type nt
        WHERE nt.status = "active"');
        if(!$typenews){
            return response(['message'=>'Success', 'data'=>'Type not found']);
        }
        $result = [];
        foreach($typenews as $type){
        if($type->created_at != null){
            $type->created_at = $contr->formatDateThat($type->created_at);
        }
        if($type->updated_at!=null){
            $type->updated_at = $contr->formatDateThat($type->updated_at);
        }
        array_push($result,$type);
        }

        return response(['message'=>'Success', 'data'=>$result],200);

    }

    public function indexTypebyID($id)
    {
        $contr = new Content_v2_Controller;
        //$type = News_type::where('id', $id)->get()->first();
        $type = DB::select('SELECT *
        FROM news_type nt
        WHERE nt.id = ?',[$id]);
        if(!$type){
            return response(['message'=>'Success', 'data'=>'Type not found']);
        }
        $result = [];
        foreach($type as $news_type){
        if($news_type->created_at != null){
                $news_type->created_at = $contr->formatDateThat($news_type->created_at);
            }
        if($news_type->updated_at!=null){
                $news_type->updated_at = $contr->formatDateThat($news_type->updated_at);
            }
        array_push($result, $news_type);
        }
        return response(['message'=>'Success', 'data'=>$result],200);
    }

    public function indexLog()
    {
        $log = News_type_log::all();
        if($log != null){
            return response(['message'=>'Success', 'data'=>$log]);
        }else{
            return response(['message'=>'Success', 'data'=>'News_type_log not found']);
        }
    }
}
