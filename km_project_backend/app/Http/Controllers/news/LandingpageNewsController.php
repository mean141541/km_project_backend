<?php

namespace App\Http\Controllers\news;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class LandingpageNewsController extends Controller
{
    public function news_count_with_view()
    {
       $news_list = DB::select("
       SELECT id AS id,title AS title,pic_path AS image,detail AS detail,view AS view,tag AS TAG
       FROM sub_subject
       WHERE status = 'active'
       ORDER BY view desc
       LIMIT 10
       ");
       return response(['message'=>'success','data'=>$news_list],200);
    }
    public function news_count_with_date_upload()
    {
        $news_list = DB::select("
        SELECT id AS id,title AS title,pic_path AS image,detail AS detail,view AS view,tag AS TAG, updated_at AS date
        FROM sub_subject
        WHERE status = 'active' 
        ORDER BY updated_at desc
        LIMIT 10
        ");
        return response(['message'=>'success','data'=>$news_list],200);
    }
}
