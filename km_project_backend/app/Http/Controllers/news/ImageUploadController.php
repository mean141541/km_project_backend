<?php

namespace App\Http\Controllers\news;

use App\Http\Controllers\Controller;
use App\News;
use App\News_picture;
use App\News_picture_log;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use App\Http\Controllers\content\Content_v2_Controller;

use function GuzzleHttp\Promise\all;

class ImageUploadController extends Controller
{
    public function keeplog($user_id, $action, $detail, $type_id)
    {
        $log = new News_picture_log();
        $log->user_id = $user_id;
        $log->action = $action;
        $log->detail = $detail;
        $log->picture_id = $type_id;
        $log->save();

    }

    public function imageUpload(Request $request ,$id)
    {
        $action = 'upload';
        if(! (isset($request->image)) &&
            (gettype($request->image == 'image|mimes:jpeg,png,jpg,svg|max:2048'))){
                return response([
                    'message'=>'Image does not exist',
                    'data'=>null
                ],400);
        }
        if(! (isset($request->type_imageName)) &&
            (gettype($request->type_imageName == 'string|max:30'))){
                return response([
                    'message'=>'type_imageName does not exist',
                    'data'=>null
                ],400);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                $news_type_image = $request->type_imageName;
                $count_image = 1;
                $imageName = $news_type_image.'_'.date('YmdHis').'_'.$count_image.'.'.$request->image->extension();
                $request->image->move(public_path('img/news'), $imageName);
                $file_path_to_database = '/img/news/'.$imageName;

                $imagePath = new News_picture();
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $imagePath->news_id = $id;
                    $imagePath->path_pic = $file_path_to_database;
                    $imagePath->status = "active";
                    $imagePath->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    ImageUploadController::keeplog($user_id->id, $action, $imagePath->path_pic, $imagePath->id);
                    return response(['message'=>'Success','data'=>$imagePath],200);
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader'],200);
                }

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }

    }

    public function imageDelete(Request $request, $id)
    {
        $action = 'delete';
        $image = News_picture::where('id',$id)->get()->first();
        if (!$image){
            return response(['message'=>'Success', 'data'=>'This ID not found'],422);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $image->status = 'reject';
                    $image->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    ImageUploadController::keeplog($user_id->id, $action, $image->path_pic, $image->id);
                    return response(['message'=>'Success', 'data'=>$image],200);
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader'],200);
                }

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }
    }

    public function imageEdit(Request $request, $id)
    {
        $action = 'edit';
        if(! (isset($request->image)) &&
            (gettype($request->image == 'image|mimes:jpeg,png,jpg,svg|max:2048'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Image does not exist'
                ],400);
        }
        if(! (isset($request->type_imageName)) &&
            (gettype($request->type_imageName == 'string|max:30'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'type_imageName does not exist'
                ],400);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                $news_type_image = $request->type_imageName;
                $count_image = 1;
                $imageName = $news_type_image.'_'.date('YmdHis').'_'.$count_image.'.'.$request->image->extension();
                $request->image->move(public_path('img/news'), $imageName);
                $file_path_to_database = '/img/news/'.$imageName;
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $imagePath = News_picture::where('id', $id)->get()->first();
                    $imagePath->path_pic = $file_path_to_database;
                    $imagePath->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    ImageUploadController::keeplog($user_id->id, $action, $imagePath->path_pic, $imagePath->id);
                    return response(['message'=>'Success','data'=>$imagePath],200);
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader'],200);
                }

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }
    }

    public function imageIndexAll()
    {
        $contr = new Content_v2_Controller;
        // $image = News_picture::all();
        $image = DB::select('SELECT * FROM news_picture np ORDER BY np.id DESC');
        if(!$image){
            return response(['message'=>'Success', 'data'=>'Image not found ']);
        }
        $result = [];
        foreach($image as $img){
            if($img->created_at != null){
                $img->created_at = $contr->formatDateThat($img->created_at);
            }
            if($img->updated_at != null){
                $img->updated_at = $contr->formatDateThat($img->updated_at);
            }
            array_push($result,$img);
        }
        return response(['message'=>'Success', 'data'=>$result]);
        // return response(['message'=>'Success', 'data'=>$image]);
    }

    public function imageIndex($id)
    {
        $contr = new Content_v2_Controller;
        // $image = News_picture::where('id',$id)->get()->first();
        $image = DB::select('SELECT * FROM news_picture np WHERE np.id = ? ',[$id]);
        if(!$image){
            return response(['message'=>'Success', 'data'=>'Image not found ']);
        }
        $result = [];
        foreach($image as $img){
            if($img->created_at != null){
                $img->created_at = $contr->formatDateThat($img->created_at);
            }
            if($img->updated_at != null){
                $img->updated_at = $contr->formatDateThat($img->updated_at);
            }
            array_push($result,$img);
        }
        return response(['message'=>'Success', 'data'=>$result]);
        // return response(['message'=>'Success', 'data'=>$image]);
    }

    public function approveImage(Request $request, $id)
    {
        $request->validate([
            'approve' => 'required',
        ]);
        $news = News_picture::where('id',$id)->get()->first();
        if (!$news){
            return response(['message'=>'Can not find this ID'],422);
        }
        if($request->approve == 'Allowed') {
            $news->status = 'active';
            $news->save();
            return response(['message'=>'Approved','data'=>$news],200);
        }
        else{
            $news->status = 'reject';
            $news->save();
            return response(['message'=>'Not approved', 'data'=>$news],200);
        }
    }

    public function indexLog()
    {
        $log = News_picture_log::all();
        if($log != null){
            return response(['message'=>'Success', 'data'=>$log]);
        }else{
            return response(['message'=>'Success', 'data'=>'News_picture_log not found']);
        }
    }
    public function uploadImageNews(Request $request,$i,$path_pic)
    {
        $type_content_image = $path_pic.'_';
        $count_image_content = $i;
        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
        $request->image->move(public_path('img/'.$path_pic), $imageName);
        $file_path_to_database = '/img/'.$path_pic.'/'.$imageName;

        // return $file_path_to_database;
        return response(['message'=>'success','data'=>$file_path_to_database],200);

    }

    public function saveImg($img_path,$news_id)
    {
        $img = new News_picture();
        $img->news_id = $news_id;
        $img->path_pic = $img_path;
        $img->status = 'active';
        $img->save();
    }

    public function saveImgEdit($img_path,$id)
    {
        $img = News_picture::where('id',$id)->get()->first();
        // $img->news_id = $news_id;
        $img->path_pic = $img_path;
        // $img->status = 'active';
        $img->save();
    }
}
