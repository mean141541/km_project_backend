<?php

namespace App\Http\Controllers\news;

use App\Http\Controllers\Controller;
use App\Http\Controllers\content\Content_v2_Controller;
use App\News;
use App\News_picture;
use App\News_type;
use App\News_log;
use App\User;
use DB;
use Illuminate\Http\Request;
use Closure;

class NewsController extends Controller
{
    public function keeplog($user_id, $action, $detail, $news_id)
    {
        $log = new News_log;
        $log->user_id = $user_id;
        $log->action = $action;
        $log->detail = $detail;
        $log->news_id = $news_id;
        $log->save();

    }
    public function indexAll()
    {
        $contr = new Content_v2_Controller;
        // return 'test';
        $news = DB::select('SELECT n.id AS id, nt.type_name, n.title, n.detail, n.created_at, n.updated_at
        FROM news_type nt, news n
        WHERE n.type_id = nt.id AND n.status = "active"
        ORDER BY n.id DESC');
        // return $news;
        $news_result = [];
        foreach($news as $newsdata){
            $newspicture = DB::select('SELECT np.news_id, np.path_pic AS pathpic
            FROM news_type nt, news n, news_picture np
            WHERE nt.id = n.type_id AND n.id = np.news_id AND n.status = "active" AND np.status = "active"');

            $newsdata->picture =[];
            foreach($newspicture as $pic){
                if($newsdata->id == $pic->news_id){
                    array_push($newsdata->picture,$pic->pathpic);
                }
            }
            if($newsdata->created_at!=null){
                $newsdata->created_at = $contr->formatDateThat($newsdata->created_at);
            }
            if($newsdata->updated_at!=null){
                $newsdata->updated_at = $contr->formatDateThat($newsdata->updated_at);
            }
            array_push($news_result,$newsdata);
        }
        if($news_result == null){
            return response(['message'=>'Success', 'data'=>'News not found']);
        }
        else{
            return response(['message'=>'Success','data'=>$news_result],200);
        }
    }

    public function newsAll()
    {
        // return 'test';
        $news = DB::select('SELECT n.id AS id, nt.type_name, n.title, n.detail, n.created_at, n.updated_at, n.status
        FROM news_type nt, news n
        WHERE n.type_id = nt.id ORDER BY id DESC');

        return response(['message'=>'success','data'=>$news],200);

    }

    public function indexNews()
    {
        $contr = new Content_v2_Controller;
        $news = DB::select('SELECT n.id AS id, nt.type_name, n.title, n.detail, n.created_at, n.updated_at
        FROM news_type nt, news n
        WHERE n.type_id = 1 AND nt.id = 1 AND n.status = "active"
        ORDER BY n.id DESC');
        $news_result = [];
        foreach($news as $newsdata){
            $newspicture = DB::select('SELECT np.news_id, np.path_pic AS pathpic
            FROM news_type nt, news n, news_picture np
            WHERE nt.id = n.type_id AND n.id = np.news_id AND n.status = "active" AND np.status = "active"');

            $newsdata->picture =[];
            foreach($newspicture as $pic){
                if($newsdata->id == $pic->news_id){
                    array_push($newsdata->picture,$pic->pathpic);
                }
            }
            if($newsdata->created_at!=null){
                $newsdata->created_at = $contr->formatDateThat($newsdata->created_at);
            }
            if($newsdata->updated_at!=null){
                $newsdata->updated_at = $contr->formatDateThat($newsdata->updated_at);
            }
            array_push($news_result,$newsdata);
        }
        if($news_result == null){
            return response(['message'=>'Success', 'data'=>'News not found']);
        }
        else{
            return response(['message'=>'Success','data'=>$news_result],200);
        }
    }

    public function insertNews(Request $request)
    {
        $action = 'create';
        if(! (isset($request->type_id)) &&
            (gettype($request->type_id == 'int'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'type_id does not exist'
                ],400);
        }
        if(! (isset($request->title)) &&
            (gettype($request->title == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Title does not exist'
                ],400);
        }
        if(! (isset($request->detail)) &&
            (gettype($request->datail == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Detail does not exist'
                ],400);
        }

        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                // return $data;
                $news = new News();
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $news->type_id = $request->type_id;
                    $news->title = $request->title;
                    $news->detail = $request->detail;
                    $news->status = "active";
                    $news->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    NewsController::keeplog($user_id->id, $action, 'Title: "'.$news->title.'" Datail: "'.$news->detail.'"', $news->id);
                    return response(['message'=>'Success', 'data'=>$news],200);

                }else{
                    return response(['message'=>'Unsuccess', 'data'=>'You are not Admin or Leader']);
                }
            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }
    }

    public function editNews(Request $request , $id)
    {
        $action = 'edit';
        $news_old = News::where('id',$id)->get()->first();
        $news = News::where('id',$id)->get()->first();
        if (!$news){
            return response(['message'=>'Success', 'data'=>'This ID not found'],422);
        }
        if(! (isset($request->type_id)) &&
            (gettype($request->type_id == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Type_id does not exist'
                ],400);
        }
        if(! (isset($request->title)) &&
            (gettype($request->title == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Title does not exist'
                ],400);
        }
        if(! (isset($request->detail)) &&
            (gettype($request->detail == 'string'))){
                return response([
                    'message'=>'Unsuccess',
                    'data'=>'Detail does not exist'
                ],400);
        }

        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    if($news->status == 'active'){
                        $news->type_id = $request->type_id;
                        $news->title = $request->title;
                        $news->detail = $request->detail;
                        $news->save();
                        $user_id = User::where('email',$data['email'])->get()->first();
                        NewsController::keeplog($user_id->id, $action,'Change : "'.$news_old->title.'","'.$news_old->detail.'" : To : "'.$news->title.'", "'.$news->detail.'" ', $news->id);
                        return response(['message'=>'Success', 'data'=>$news],200);
                    } else{
                        return response(['message'=>'Unsuccess', 'data'=>'You can not fix, News status: not active',422]);
                    }
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader']);
                }
            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }


    }

    public function deleteNews(Request $request, $id)
    {
        $action = 'delete';
        $news = News::where('id',$id)->get()->first();
        if (!$news){
            return response(['message'=>'Success', 'data'=>'This ID not found'],422);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
                if ($data['permission'] == 'Admin' || $data['permission'] == 'Leader'){
                    $news->status = 'reject';
                    $news->save();
                    $user_id = User::where('email',$data['email'])->get()->first();
                    NewsController::keeplog($user_id->id, $action, '"'.$news->title.'","'.$news->detail.'"', $news->id);
                    return response(['message'=>'Success', 'data'=>$news],200);
                }else{
                    return response(['message'=>'Unsuccess','data'=>'You are not Admin or Leader']);
                }
            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'date'=>$th
                ],401);
            }
        }

    }



    public function index($id)
    {
        $contr = new Content_v2_Controller;
        $news = DB::select('SELECT *
        FROM news_type nt, news n
        WHERE n.type_id = nt.id AND n.id = ?',[$id]);
        $news_result = [];
        foreach($news as $newsdata){
            $newspicture = DB::select('SELECT np.news_id, np.path_pic AS pathpic, np.status
            FROM news_type nt, news n, news_picture np
            WHERE nt.id = n.type_id AND n.id = np.news_id AND np.news_id =?',[$id]);

            $newsdata->picture =[];
            foreach($newspicture as $pic){
                if($newsdata->id == $pic->news_id){
                    array_push($newsdata->picture,$pic->pathpic);
                    array_push($newsdata->picture,$pic->status);
                }
            }
            if($newsdata->created_at != null){
                    $newsdata->created_at = $contr->formatDateThat($newsdata->created_at);
                }
            if($newsdata->updated_at != null){
                    $newsdata->updated_at = $contr->formatDateThat($newsdata->updated_at);
                }
            array_push($news_result,$newsdata);
        }
        if(!$news){
            return response(['message'=>'Success', 'data'=>'News not found']);
        }
        return response(['message'=>'Success','data'=>$news_result],200);
    }

    public function approveNews(Request $request, $id)
    {
        $request->validate([
            'approve'=>'required'
        ]);
        $news = News::where('id',$id)->get()->first();
        if($request->approve == 'Allowed') {
            $news->status = 'active';
            $news->save();
            return response(['message'=>'Approved','data'=>$news],200);
        }
        else{
            $news->status = 'reject';
            $news->save();
            return response(['message'=>'Not approved','data'=>$news],200);
        }

    }

    public function indexAll_activity()
    {
        $contr = new Content_v2_Controller;
        // return 'test';
        $news = DB::select('SELECT n.id AS id, nt.type_name, n.title, n.detail, n.created_at, n.updated_at
        FROM news_type nt, news n
        WHERE n.type_id = 2 AND nt.id = 2 AND n.status = "active"
        ORDER BY n.id DESC');
        // return $news;
        $news_result = [];
        foreach($news as $newsdata){
            $newspicture = DB::select('SELECT np.news_id, np.path_pic AS pathpic
            FROM news_type nt, news n, news_picture np
            WHERE nt.id = n.type_id AND n.id = np.news_id AND n.status = "active" AND np.status = "active"');

            $newsdata->picture =[];
            foreach($newspicture as $pic){
                if($newsdata->id == $pic->news_id){
                    array_push($newsdata->picture,$pic->pathpic);
                }
            }
            if($newsdata->created_at!=null){
                $newsdata->created_at = $contr->formatDateThat($newsdata->created_at);
            }
            if($newsdata->updated_at!=null){
                $newsdata->updated_at = $contr->formatDateThat($newsdata->updated_at);
            }
            array_push($news_result,$newsdata);
        }
        if($news_result == null){
            return response(['message'=>'Success', 'data'=>'Activity not found']);
        }
        else{
            return response(['message'=>'success','data'=>$news_result],200);
        }
    }

    public function indexLog()
    {
        $log = News_log::all();
        if($log != null){
            return response(['message'=>'Success', 'data'=>$log]);
        }else{
            return response(['message'=>'Success', 'data'=>'News_log not found']);
        }
    }


}
