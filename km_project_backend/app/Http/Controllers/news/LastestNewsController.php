<?php

namespace App\Http\Controllers\news;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LastestNewsController extends Controller
{
    public function ActivityNew()
    {        
        try{
            $url = 'https://www.thaiware.com/rss/rss_latestPost_news.php';
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
            $response = curl_exec ($ch);
            $err = curl_error($ch);  //if you need
            curl_close ($ch);
            $text=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $response);
            $xml = simplexml_load_string($text);
            
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
            return response(['message'=>'success','data'=>$array['channel']['item']],200);
        }catch(\Exception $th){
            return response(['message'=>'service is not available','data'=>null],500);
        }
    }
}
