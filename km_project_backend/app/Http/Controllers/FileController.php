<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function getSummernote()

    {

        return view('testing.test2');

    }

    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function postSummernote(Request $request)
    {
        $this->validate($request, [

            'detail' => 'required',

        ]);

        $detail = $request->input('detail');

        $dom = new \DomDocument();
        $dom->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>'.$detail);

        $images = $dom->getElementsByTagName('img');

        foreach($images as $k => $img){

            $data = $img->getAttribute('src');
            try{
                list(, $data)      = explode(';', $data);
                list(, $data)      = explode(',', $data);

                $data = base64_decode($data);

                $image_name= "/img/upload/" . time().$k.'.png';
                $path = str_replace("/",'\\',$image_name);
                file_put_contents(public_path($path), $data);

                $img->removeAttribute('src');
                $img->setAttribute('src', url($image_name));
            }catch (\Exception $th) {}
        }

        $detail = $dom->saveHTML();
        $detail = explode('<body>', $detail, 3);
        $detail = explode('</body>', $detail[1], 2);

        return ($detail[0]);
    }
}
