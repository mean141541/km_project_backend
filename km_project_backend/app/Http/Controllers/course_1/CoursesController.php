<?php

namespace App\Http\Controllers\course;
use Illuminate\Http\Request;
use App\Course;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['user_id' => 'required', 'title' => 'required' , 'detail' => 'required' ,   'status' => 'required' ,   'path_pic' => 'required' ]);                    //ตรวจสอบการเพิ่มข้อมูล
        $course = new Course (
        [
        'user_id' => $request->get('user_id'),
        'title' => $request->get ('title'),
        'detail' => $request->get ('detail'),
        'status' => $request->get ('status'),
        'path_pic' => $request->get ('path_pic')
         ]
        );
          $testuser->save(); //คำสั่งบันทึกข้อมูล
          return redirect()->route('Admin.index')->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
