<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;
use App\Http\Controllers\covid\CovidController;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function index(Request $request)
    {
        $access_validate = $request->bearerToken();
        $data = unserialize(base64_decode($access_validate));
        return response()->json(['success'=>$data],200);

    }
}
