<?php

namespace App\Http\Controllers\redirect_from_front;

use Cookie;
use Redirect;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Http\Controllers\AuthController;

class Redirect_from_front_Controller extends Controller
{
    public function index($token)
    {
        try{
            $u_and_p = Crypt::decryptString($token);
            $u_and_p = Crypt::decryptString($u_and_p);
            $u_and_p = unserialize(base64_decode($u_and_p));
        }catch (\Exception $th){
            // dd($th);
            abort(403, 'Unauthorized action.');
        }
            $request = new \Illuminate\Http\Request();
            $request->replace($u_and_p);
            $auth = new AuthController;
            $data = $auth->login($request);
            $data = $data->original;
            if(!isset($data['access_token'])){
                abort(403, 'Unauthorized action.');
            }else{
                setcookie('token', $data['access_token'], time()+60*60*24*365);
                Session::put('token', $data['access_token']);
                // dd(Session::get('token'));
                return redirect()->route('admin.index');
            }


    }
    public function logout()
    {
        Session::flush();
        return redirect()->to(env('logout_url').'/logout');
    }
}
