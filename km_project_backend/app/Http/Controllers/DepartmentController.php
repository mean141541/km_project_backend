<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;

class DepartmentController extends Controller
{
    public function departmentindex()
    {
        $department = Department::all();

        return response(['message'=>'success','data'=>$department],200);
    }
}
