<?php

namespace App\Http\Controllers\courses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Courses;
use App\Courses_table_log;
use App\User;
class CoursesImgController extends Controller
{
    

    private static function keep_log($user_id, $action) # การทำเก็บไฟล์ log :)
    {
        
        $log_courses = new Courses_table_log();
        $log_courses->user_id = $user_id;
        $log_courses->action = $action;      
        $log_courses->save();
        return $log_courses;
    }
    
    public function imageEdit(Request $request, $id) # แก้ไขรูปภาพ(แต่ไม่ได้เอารูปใหม่มาแทนที่เก่าเป็นการอัพรูปใหม่ไปแทนไ่ม่ได้ลบไฟล์เก่า) :)
    {
        $access_validate = $request->bearerToken();  # การตรวจสอบ Token :)
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }

        $user_id = User::where('email',$data['email'])->get()->first();   #ตรวจสอบ Email :)
        if(!$user_id){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }


        if (
            !(isset($request->path_pic)) &&   
            (gettype($request->path_pic == 'string'))
        ) {
            return response([
                'message' => 'path_pic does not exist',
                'data' => null
            ], 400);
        }
        $user_id = $user_id->id;
        $courses = Courses::Where('id',$id)->get()->first();
       

        if(!$request->path_pic == null){            # การทำอัพโหลดรูป :)
            $type_courses_image = 'Courses_';
            $count_image_courses = 1;
            $imageName = $type_courses_image.date('YmdHis').'_'.$count_image_courses.'.'.$request->path_pic->extension();
            $request->path_pic->move(public_path('img/Courses'), $imageName);
            $file_path_to_database = '/img/Courses/'.$imageName;
            $courses->path_pic = $file_path_to_database;

        }else{
            $courses->path_pic = '';
        }
            $courses->save();
            $action = 'Update Picture Courses  '; # การทำ log :)
            CoursesImgController::keep_log($user_id, $action);  # การทำ log :)

        return response(['message'=>'courses editing success','data'=>$courses],200);
    }
    
}
