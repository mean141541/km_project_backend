<?php

namespace App\Http\Controllers\courses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Courses;
use App\Courses_table_log;
use App\User;
class CoursesController extends Controller
{

    private static function keep_log($user_id, $action)   # การทำ log :)
    {
        // dd($request->path_pic);
        // $request->validate([
        //     'title'=>'required',
        //     'detail'=>'required',
        //     'status'=>'required',
        //     'user_id'=>'required',
        //     'path_pic'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048'
        //     ]);
        if($request->title == null || $request->detail == null ||$request->status == null ||$request->user_id == null ||$request->path_pic == null ){
            return response(['message'=>'error','data'=>'bad request'],400);
        }
        $type_courses_image = 'Courses_';
        // dd($type_courses_image);
        $log_courses = new Courses_table_log();
        $log_courses->user_id = $user_id;
        $log_courses->action = $action;
        $log_courses->save();
        return $log_courses;
    }

    public function create(Request $request)  # สร้างข้อมูล :)

    {
        if (
            !(isset($request->title)) &&     # เช็คว่า title มีข้อมูลเป็น string ใช่ไหม ถ้าไม่่ใช่ ให้ฟ้องแจ้งเตือน :)
            (gettype($request->title == 'string'))
        ) {
            return response([
                'message' => 'title does not exist',
                'data' => null
            ], 400);
        }

        if (
            !(isset($request->detail)) &&
            (gettype($request->detail == 'string'))
        ) {
            return response([
                'message' => 'title does not exist',
                'data' => null
            ], 400);
        }
        if (
            !(isset($request->status)) &&
            (gettype($request->status == 'string'))
        ) {
            return response([
                'message' => 'Status does not exist',
                'data' => null
            ], 400);
        }

        if (
            !(isset($request->path_pic)) &&
            (gettype($request->path_pic == 'string'))
        ) {
            return response([
                'message' => 'path_pic does not exist',
                'data' => null
            ], 400);
        }
        $access_validate = $request->bearerToken();  # การทำเช็คค่า token :)
        try {
            $data = unserialize(base64_decode($access_validate));
            if($data == false){
                return response()->json([
                    'message' => 'success',
                    'data'=> 'No permission'
                ],401);
            }

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }

        $user_id = User::where('email',$data['email'])->get()->first();   # การทำเช็ค Email :)
        if(!$user_id){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }

        $type_courses_image = 'Courses_';  # การอัพโหลดรูป :)

        $count_image_courses = 1;
        $imageName = $type_courses_image.date('YmdHis').'_'.$count_image_courses.'.'.$request->path_pic->extension();
        $request->path_pic->move(public_path('img/Courses'), $imageName);
        $file_path_to_database = '/img/Courses/'.$imageName;
        $user_id = $request->user_id;
        // dd($request->user_id);
        $courses = new Courses(
            [
                'title' => $request->title,
                'detail' => $request->detail,
                'path_pic' => $file_path_to_database,
                'status' => $request->status,
                'user_id' => $request->user_id
            ]
            );
           $courses->save();
           $action = 'Create Courses = ' . $request->get('title') . '';
           CoursesController::keep_log($user_id, $action);
           return response(['message'=>'success','data'=>$courses],200);

    }
    public function Show()   # แสดงข้อมูล :)
    {
        $courses = Courses::all();
        return response(['message'=>'success','data'=>$courses],200);
    }

    public function update(Request $request,$id)    # อัพเดทข้อมูล(ไม่รวมรูปภาพ) :)
    {
        $this->validate($request,['title'=>'required','detail'=>'required','status'=>'required','user_id'=>'required']);
        // $type_courses_image = $request->type_imageName;
        // $count_image_content = 1;
        // $imageName = $type_courses_image.date('YmdHis').'_'.$count_image_content.'.'.$request->path_pic->extension();
        // $request->path_pic->move(public_path('img/Courses'), $imageName);
        // $file_path_to_database = '/img/Courses/'.$imageName;

        $courses = Courses::find($id);
        $courses->title = $request->get('title');
        $courses->detail = $request->get('detail');
        $courses->status = $request->get('status');
        $courses->user_id = $request->get('user_id');
        // $courses->path_pic = $request->$file_path_to_database;



        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }

        $user_id = User::where('email',$data['email'])->get()->first();
        if(!$user_id){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        if (
            !(isset($request->title)) &&
            (gettype($request->title == 'string'))
        ) {
            return response([
                'message' => 'title does not exist',
                'data' => null
            ], 400);
        }

        if (
            !(isset($request->detail)) &&
            (gettype($request->detail == 'string'))
        ) {
            return response([
                'message' => 'Detail does not exist',
                'data' => null
            ], 400);
        }

        if (
            !(isset($request->status)) &&
            (gettype($request->status == 'string'))
        ) {
            return response([
                'message' => 'Status does not exist',
                'data' => null
            ], 400);
        }

        $user_id = $user_id->id;

        $courses = Courses::find($id);
        $courses->title = $request->title;
        $courses->detail = $request->detail;
        $courses->status = $request->status;
        $courses->user_id = $user_id;
        //   return $courses;

        $courses->save();

       $action = 'Update Title = ' . $request->get('title') .'';
        CoursesController::keep_log($user_id, $action);
        return response(['message'=>'success','data'=>$courses],200);
    }

    public function delete(Request $request, $id)   # ลบข้อมูล :)
    {

        // $courses = Courses::Where('id', $id)->get()->first();
        $courses = Courses::find($id);
        $courses->status = 'reject';


        $access_validate = $request->bearerToken();
        try {
            $data = unserialize(base64_decode($access_validate));

        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }

        $user_id = User::where('email',$data['email'])->get()->first();
        if(!$user_id){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }
        // $courses = Courses::Where('id', $id)->get()->first();

        $user_id = $user_id->id;
        $courses = Courses::find($id);
        $courses->status = 'reject';

        $courses->save();
        $action = 'Delete Status = reject ';
        CoursesController::keep_log($user_id, $action);
        return response(['message'=>'success','data'=>$courses],200);
    }

    // public function delete($id=null)
    // {
    //     $courses_vdo = Courses_vdo::where('id',$id)->get()->first();
    //     if (!$courses_vdo){
    //         return response(['message'=>'Can not find this ID'],422);
    //     }
    //     $courses_vdo->status = 'reject';
    //     $courses_vdo->save();
    //     return response(['message'=>'Delete Courses Vdo Success','data'=>$courses_vdo],200);
    // }









}
