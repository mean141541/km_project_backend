<?php

namespace App\Http\Controllers\user;

use App\User;
use App\Users_log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    private static function keep_log ($user_id,$action){
        $log = new Users_log();
        $log->user_id = $user_id;
        $log->action = $action;
        $log->save();
    }
    public function edit_permission(Request $request)
    {
        // try{
            $access_validate = $request->bearerToken();
            try {
                $data = unserialize(base64_decode($access_validate));

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$access_validate
                ],401);
            }
            $request->validate([
                'email_edit' => 'required|string|max:255',
                'permission_edit' => 'required|string|max:255'
            ]);
            $user = User::where('email',$request->email_edit)->get()->first();
            $user_old_permission = $user->permission;
            if($request->permission_edit != 'Admin' &&
            $request->permission_edit !== 'User' &&
            $request->permission_edit !== 'Leader'){
                return response([
                    'message'=>'error',
                    'data'=>'permission_edit invalid'
                    ],422);
            }
            if(! $user){
                return response([
                    'message'=>'error',
                    'data'=>'this user does not exist'
                    ],422);
            }else{
                $user->permission = $request->permission_edit;
                $user->save();
                $user_id = User::where('email',$data['email'])->get()->first()->id;
                PermissionController::keep_log($user_id,'Change permission of "'.$request->email_edit.'" from "'.$user_old_permission.'" to "'.$request->permission_edit.'"');
                return response([
                    'message'=>'success',
                    'data'=>[
                        'email_edit'=>$request->email_edit,
                        'permission_edit'=>$request->permission_edit
                        ]
                    ],200);
            }
        // } catch (\Exception $th){
        //     return response([
        //         'message'=>'error',
        //         'data'=>'service is not available'
        //     ],500);
        // }
    }
}
