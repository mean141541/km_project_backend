<?php

namespace App\Http\Controllers\user;

use File;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use App\User;
use App\Users_log;
use DB;

class UsersController extends Controller
{
    private static function keep_log ($user_id,$action){
        $log = new Users_log();
        $log->user_id = $user_id;
        $log->action = $action;
        $log->save();
    }
    public function get_all_user()
    {
        try{
            $user = User::all();
            return response([
                'message'=>'success',
                'data'=>$user
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }
    }
    public function delete_user(Request $request)
    {
        try{
            $access_validate = $request->bearerToken();
            try {
                $data = unserialize(base64_decode($access_validate));

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$access_validate
                ],401);
            }
            $request->validate([
                "email_delete" => "required|string|email|max:255",
            ]);
            $user = User::where('email',$request->email_delete)->get()->first();
            if (!$user){
                return response([
                    'message'=>'this user does not exist',
                    'data'=>[
                        'email_delete'=>$request->email_delete
                        ]
                    ],422);
            }else{
                if($data['email'] == $request->email_delete){
                    return response([
                        'message'=>'Can not delete user yourself',
                        'data'=>[
                            'email_delete'=>$request->email_delete
                            ]
                        ],422);
                }else{
                    $filename = $user->avatar;
                    File::delete(public_path($filename));
                    DB::delete('delete FROM users where email = ?', [$request->email_delete]);
                    $user_id = User::where('email',$data['email'])->get()->first()->id;
                    UsersController::keep_log($user_id,'Delete "'.$request->email_delete.'" ');
                    return response([
                        'message'=>'success',
                        'data'=>[
                            'email_delete'=>$request->email_delete
                            ]
                        ],200);
                }
            }
        } catch (\Exception $th){
            return response(
                ['message'=>'service is not available',
                'data'=>null
            ],500);
        }
    }
    public function edit_email(Request $request)
    {
        // try{
            $access_validate = $request->bearerToken();
            try {
                $data = unserialize(base64_decode($access_validate));

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$access_validate
                ],401);
            }
            $request->validate([
                'email_origin'=>'required|string|max:255',
                'email_new'=>'required|string|email|max:255|unique:users,email'
            ]);
            $user = User::where('email',$request->email_origin)->get()->first();
            $user_old_email = $user->email;
            if (!$user){
                return response([
                    'message'=>'this user does not exist',
                    'data'=>[
                        'email_origin'=>$request->email_origin,
                        'email_new'=>$request->email_new
                        ]
                    ],422);
            }
            $user->email = $request->email_new;
            $user->save();
            $user_id = User::where('email',$data['email'])->get()->first()->id;
            UsersController::keep_log($user_id,'Change email from "'.$user_old_email.'" to "'.$request->email_new.'"');
            return response(['message'=>'success',
                'data'=>[
                    'email_origin'=>$request->email_origin,
                    'email_new'=>$request->email_new
                    ]
                ],200);
        // } catch (\Exception $th){
        //     return response([
        //         'message'=>'error',
        //         'data'=>'service is not available'
        //     ],500);
        // }
    }
    public function edit_name(Request $request)
    {
        $access_validate = $request->bearerToken();
            try {
                $data = unserialize(base64_decode($access_validate));

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$access_validate
                ],401);
            }
        // try{
            $request->validate([
                'email_origin'=>'required|string|max:255',
                'name_new'=>'required|string|max:255'
            ]);
            $user = User::where('email',$request->email_origin)->get()->first();
            $user_old_name = $user->name;
            if (!$user){
                return response([
                    'message'=>'this user does not exist',
                    'data'=>[
                        'email_origin'=>$request->email_origin,
                        'name_new'=>$request->name_new
                        ]
                    ],422);
            }
            $user->name = $request->name_new;
            $user->save();
            $user_id = User::where('email',$data['email'])->get()->first()->id;
            UsersController::keep_log($user_id,'Change name from "'.$user_old_name.'" to "'.$request_name_new.'"');
            return response([
                'message'=>'success',
                'data'=>[
                    'email_origin'=>$request->email_origin,
                    'name_new'=>$request->name_new
                    ]
                ],200);
        // } catch (\Exception $th){
        //     return response([
        //         'message'=>'error',
        //         'data'=>'service is not available'
        //     ],500);
        // }
    }
    public function edit_password(Request $request)
    {
        try{
            $access_validate = $request->bearerToken();
            try {
                $data = unserialize(base64_decode($access_validate));

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$access_validate
                ],401);
            }
            $request->validate([
                'password'=>'required|string|max:255',
                'new_password'=>'required|string|max:255'
            ]);
            $user = User::where('email',$data['email'])->get()->first();
            $user_id = $user->id;
            if (!$user){
                return response([
                    'message'=>'this user does not exist',
                    'data'=>null
                ],422);
            }
            $credentials =  request([$data['email'],'password']);
            if (!Hash::check($request->password, $user->password))
            {
                return response()->json([
                    'message' => 'Unauthorized',
                    'data'=>null
                ], 401);
            }
            $user->password = bcrypt($request->new_password);
            $user->save();
            UsersController::keep_log($user_id,'Change password');
            return response([
                'message'=>'success',
                'data'=>null
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }
    }

}
