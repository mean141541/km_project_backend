<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Users_log;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Illuminate\Contracts\Session\Session;

class AuthController extends Controller
{
    private static function keep_log ($user_id,$action){
        $log = new Users_log();
        $log->user_id = $user_id;
        $log->action = $action;
        $log->save();
    }
    public function upload_avatar(Request $request)
    {
        try{
            $access_validate = $request->bearerToken();
            try {
                $data = unserialize(base64_decode($access_validate));

            } catch (\Exception $th) {
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>null
                ],401);
            }
            $user = User::where('email',$data['email'])->get()->first();
            if (!$user){
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$data
                ],401);
            }
            if(!$request->avatar){
                return response()->json([
                    'message' => 'Does not exist avatar',
                    'data'=>null
                ],400);
            }
            $request->validate([
                'avatar' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
            ]);
            $type_content_image = 'user_';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->avatar->extension();
            $request->avatar->move(public_path('img/user_avatar'), $imageName);
            $file_path_to_database = '/img/user_avatar/'.$imageName;
            $user->avatar = $file_path_to_database;
            $user->save();
            AuthController::keep_log($user->id,'Update avatar');
            return response()->json([
                'message' => 'success',
                'data'=>$data['email']
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }

    }
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
             $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:users|max:255',
                'password' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',

            ]);
            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'lastname'=>$request->lastname,
                'permission' => 'User',
                'password' => bcrypt($request->password),
                'avatar' => ''
            ]);
            $user->save();
            AuthController::keep_log($user->id,'Register');
            return response()->json([
                'message' => 'Successfully created user!',
                'data'=>[
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'lastname'=>$request->lastname,
                    'permission'=>'User',
                    'avatar'=>''
                    ]
                ], 201);

    }
    public function login(Request $request)
    {

            $request->validate([
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|max:255'
            ]);
            $validate_user = User::where('email',$request->email)->get()->first();
            if(!$validate_user){
                return response()->json([
                    'message' => 'Does not exist this email',
                    'data'=>[
                        'email'=>$request->email
                    ]
                ], 401);
            }
            $credentials = request(['email', 'password']);
            $password_db = $validate_user->password;
            // dd($request->password,$password_db);
            if(!Hash::check($request->password, $password_db)){
                AuthController::keep_log($validate_user->id,'login fail');
                return response()->json([
                    'message' => 'Unauthorized',
                    'data'=>[
                        'email'=>$request->email
                    ]
                ], 401);
            }
            $tokenAccess = [
                'permission'=> $validate_user->permission,
                'expire'=> Carbon::now()->addDays(1),
                'email' => $validate_user->email
            ];
            $username_password = [
                'email'=>$request->email,
                'expire'=> Carbon::now()->addDays(1),
                'password'=>$request->password
            ];
            $u_and_p =  base64_encode(serialize(($username_password)));
            $encrypted = base64_encode(serialize(($tokenAccess)));
            $u_and_p = Crypt::encryptString($u_and_p);
            $u_and_p = Crypt::encryptString($u_and_p);
            AuthController::keep_log($validate_user->id,'login success');
            if($validate_user->permission == 'Admin'){
                return response()->json([
                    'access_token' => $encrypted,
                    'token_type' => 'Bearer',
                    'name'=> $validate_user->name,
                    'lastname'=>$validate_user->lastname,
                    'email'=> $validate_user->email,
                    'permission'=> $validate_user->permission,
                    'avatar'=> $validate_user->avatar,
                    'admin_redirect'=>$u_and_p
                ]);
            }else{
                return response()->json([
                    'access_token' => $encrypted,
                    'token_type' => 'Bearer',
                    'name'=> $validate_user->name,
                    'lastname'=>$validate_user->lastname,
                    'email'=> $validate_user->email,
                    'permission'=> $validate_user->permission,
                    'avatar'=> $validate_user->avatar
                ]);
            }

    }
    
}
