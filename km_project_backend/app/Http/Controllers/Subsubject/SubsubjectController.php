<?php

namespace App\Http\Controllers\Subsubject;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sub_subject;
use App\Subject;

class SubsubjectController extends Controller
{
    public function subsubjectindex($id=null)
    {
        try{
            $sub_subject = Sub_subject::Where(['subject_id'=> $id ,'status'=> 'active'])->get()->sortByDesc('view');
            if($sub_subject == null){
                return response(['message'=>'success','data'=>$sub_subject],200);
            }else{
                return response(['message'=>'success','data'=>$sub_subject],200);
            }
        }catch(\Exception $ex){
            return response([
                'message'=>'error',
                'data'=>$ex
            ],500);
        }
    }

    public function showsubsubject($id=null)
    {
        try{
            $sub_subject = Sub_subject::Where('id',$id)->get()->first();

            if($sub_subject == null){
                return response(['message'=>'success','data'=>$sub_subject],200);
            }else{
                if($sub_subject->status == 'active'){
                    $sub_subject->view = $sub_subject->view + 1;
                    $sub_subject->save();
                    return response(['message'=>'success','data'=>$sub_subject],200);
                }
                else{
                    return response(['message'=>'sub_subject deleted','data'=>$sub_subject],200);
                }
            }
        }catch(\Exception $ex){
            return response([
                'message'=>'error',
                'data'=>$ex
            ],500);
        }
    }

    public function editsubsubject(Request $request, $id=null)
    {
        $data = request()->validate(
            [
            'title'=>'required|string|max:100',
            'pic_path'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'detail'=>'required|max:2000',
            'tag'=>'required|string|max:100',
            ]
        );
        if(! (isset($request->title)) &&
        (gettype($request->title == 'string'))){
            return response([
                'message'=>'Title does not exist',
                'data'=>null
            ],400);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
            'message' => 'Unauthorization',
            'data'=>$access_validate
        ],401);
        }else{
            try{
                $data = unserialize(base64_decode($access_validate));
                $sub_subject = Sub_subject::Where('id',$id)->get()->first();
                $sub_subject->subject_id = $request->input('subject_id');
                $sub_subject->title = $request->input('title');
                $sub_subject->detail = $request->input('detail');
                $sub_subject->tag = $request->input('tag');
                $sub_subject->created_at = date('Y-m-d H:i:s');
                $sub_subject->updated_at = date('Y-m-d H:i:s');

                if($sub_subject->title != null || $sub_subject->detail != null || $sub_subject->tag != null){

                        if(!$request->pic_path == null){
                            $type_content_image = 'sub_subject_pic';
                            $count_image_content = 1;
                            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->pic_path->extension();
                            $request->pic_path->move(public_path('img/sub_subject'), $imageName);
                            $file_path_to_database = '/img/sub_subject/'.$imageName;
                            $sub_subject->pic_path = $file_path_to_database;
                        }else{
                            $sub_subject->pic_path = '';
                        }
                        if ($data['permission'] == 'Admin'){
                            $sub_subject->status = $request->status;
                        }
                        if ($data['permission'] == 'Leader'){
                            $sub_subject->status = $request->status;
                        }

                        $subject = Subject::Where('id',$sub_subject->subject_id )->get()->first();
                        if($subject == null){
                            return response(['message'=>'subject id not found','data'=>$sub_subjct],200);
                        }else{
                            $sub_subject->save();
                            return response(['message'=>'edit success','data'=>$sub_subject],200);
                        }
                }else{
                    return response(['message'=>'Sub_subject is null'],422);
                }
            }catch(\Exception $ex){
                return response([
                    'message'=>'error',
                    'data'=>$ex
                ],500);
            }
        }
    }

    public function addSub_subject(Request $request)
    {
        $data = request()->validate(
            [
            'title'=>'required|string|max:100',
            // 'pic_path'=>'image|mimes:jpeg,png,jpg,svg|max:2048',
            'detail'=>'required|max:2000',
            'tag'=>'required|string|max:100',
            ]
        );
        if(! (isset($request->title)) &&
        (gettype($request->title == 'string'))){
            return response([
                'message'=>'Title does not exist',
                'data'=>null
            ],400);
        }
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
            'message' => 'Unauthorization',
            'data'=>$access_validate
        ],401);
        }else{
            try{
                $data = unserialize(base64_decode($access_validate));
                $sub_subject = new Sub_subject;
                $sub_subject->subject_id = $request->input('subject_id');
                $sub_subject->title = $request->input('title');
                $sub_subject->detail = $request->input('detail');
                $sub_subject->view = 0;
                $sub_subject->tag = $request->input('tag');
                $sub_subject->created_at = date('Y-m-d H:i:s');
                $sub_subject->updated_at = date('Y-m-d H:i:s');

                if($sub_subject->title != null || $sub_subject->detail != null || $sub_subject->tag != null){
                    if(!$request->pic_path == null){
                        $type_content_image = 'sub_subject_pic';
                        $count_image_content = 1;
                        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->pic_path->extension();
                        $request->pic_path->move(public_path('img/sub_subject'), $imageName);                        $file_path_to_database = '/img/sub_subject/'.$imageName;
                        $sub_subject->pic_path = $file_path_to_database;
                    }else{
                        $sub_subject->pic_path = '';
                    }
                    if ($data['permission'] == 'Admin'){
                        $sub_subject->status = "active";
                    }
                    if ($data['permission'] == 'Leader'){
                        $sub_subject->status = "active";
                    }
                    $subject = Subject::Where('id',$sub_subject->subject_id )->get()->first();
                    if($subject == null){
                        return response(['message'=>'subject id not found','data'=>$sub_subject],200);
                    }else{
                        $sub_subject->save();
                        return response(['message'=>'Sub_subject add success','data'=>$sub_subject],200);
                    }
                }else{
                    return response(['message'=>'Sub_subject is null'],422);
                }

            }catch(\Exception $ex){
                return response([
                    'message'=>'error',
                    'data'=>$ex
                ],500);
            }
            }
        }

    public function deletesubsubject($id=null)
    {
        try{
            if($id == null){
                return response(['message'=>'Sub_subject id not found','data'=>$sub_subjct],200);
            }else{
                $sub_subject = Sub_subject::Where('id' ,$id)->get()->first();
                $sub_subject->status = 'delete';
                $sub_subject->save();
                return response(['message'=>'success','data'=>$sub_subject],200);
            }
            }
            catch(\Exception $ex)
            {
                return response([
                    'message'=>'error',
                    'data'=>$ex
                ],500);
            }
    }

}
