<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FileController;
use App\Http\Controllers\news\ImageUploadController;
use App\Http\Controllers\news\NewsController;
use App\Http\Controllers\news\NewstypeController;
use App\News;
use App\News_picture;
use App\News_type;
use Illuminate\Http\Request;
use DB;

class adminNewsController extends Controller
{
        // News ------------------------------------------------------------

        public function news()
        {
            $news_service = new NewsController;
            $data = json_decode($news_service->newsAll()->content());

            return view('Admin.news.show',compact('data'));
        }

        public function newsDetail($id)
        {
            $news_service = new NewsController;
            $data = json_decode($news_service->index($id)->content());
            $type = News_type::where('status', 'active')->get()->all();

            if($data->message == 'Success'){
                $data = $data->data[0];
                return view('Admin.news.detail',compact('data','type'));
            }else {
                return $this->news();
            };
        }

        public function newsUpdate(Request $request,$id)
        {
            $this->validate($request, [

                'title' => 'required',
                'type' => 'required',
                'detail' => 'required',
                'status' => 'required',

            ]);

            $title = $request->input('title');
            $type = $request->input('type');
            $detail = $request->input('detail');
            $status = $request->input('status');

            $file_service = new FileController;
            $detail = $file_service->postSummernote($request);
            // return($detail);
            $news = News::where('id',$id)->get()->first();
            $news->type_id = $type;
            $news->title = $title;
            $news->detail = $detail;
            $news->status = $status;
            $news->save();

            return redirect()->route('news.index');
        }

        public function newsCreate()
        {
            $data = News_type::where('status', 'active')->get()->all();

            return view('Admin.news.create',compact('data'));
        }

        public function newsInsert(Request $request)
        {
            // dd($request);
            $this->validate($request, [

                'title' => 'required',
                'type' => 'required',
                'detail' => 'required',
                'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

            ]);

            $status = 'active';

            $file_service = new FileController;
            $detail = $file_service->postSummernote($request);

            $news = new News();
            $news->type_id = $request->input('type');
            $news->title = $request->input('title');
            $news->detail = $detail;
            $news->status = $status;
            $news->save();

            $i = 1;
            $path_pic = 'news';
            $data = $request->image;

            if($data!= NULL ){
                foreach ($data as $img) {
                    $request->image = $img;
                    $img_service = new ImageUploadController;
                    $img_path = json_decode($img_service->uploadImageNews($request,$i,$path_pic)->content());
                    $img_service->saveImg($img_path->data,$news->id);
                    $i++;
                }
            }
            return redirect()->route('news.index');
        }

        // News Type -----------------------------------------------------------
        public function newsType()
        {
            $news_service = new NewstypeController;
            $data = json_decode($news_service->indexType()->content());
            // $data = News_type::all();
            return view('Admin.news.type',compact('data'));
        }

        public function newstypeDetail($id)
        {
            //call api get news type by id
            $news_service = new NewstypeController;
            $data = json_decode($news_service->indexTypebyID($id)->content());
            $data = $data->data[0];

            return view('Admin.news.type_detail',compact('data'));
        }

        public function newstypeUpdate(Request $request,$id)
        {
            $this->validate($request, [

                'type_name' => 'required',
                'status' => 'required',

            ]);

            $type_name = $request->input('type_name');
            $status = $request->input('status');

            $news = News_type::where('id',$id)->get()->first();
            $news->type_name = $type_name;
            $news->status = $status;
            $news->save();

            return redirect()->route('news.type');
        }

        public function newstypeCreate()
        {
            return view('Admin.news.type_create');
        }

        public function newstypeInsert(Request $request)
        {
            $this->validate($request, [

                'type_name' => 'required',

            ]);

            $type_name = $request->input('type_name');
            $status = 'active';

            $news = new News_type();
            $news->type_name = $type_name;
            $news->status = $status;
            $news->save();

            return redirect()->route('news.type');
        }

        // News Album ----------------------------------------------------------
        public function newsAlbum($id)
        {
            $data = News_picture::where('news_id',$id)->where('status','active')->get()->all();
            return view('Admin.news.img',compact('data','id'));
        }

        public function newsalbumInsert(Request $request,$id)
        {
            $this->validate($request, [

                'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

            ]);

            $i = 1;
            $path_pic = 'news';
            $data = $request->image;
            foreach ($data as $img) {
                $request->image = $img;
                $img_service = new ImageUploadController;
                $img_path = json_decode($img_service->uploadImageNews($request,$i,$path_pic)->content());
                $img_service->saveImg($img_path->data,$id);
                $i++;
            }
            return redirect()->route('news.album',$id);
        }

        public function newsalbumDelete($id)
        {
            $news = News_picture::where('id',$id)->get()->first();
            DB::update('UPDATE news_picture SET status = ? WHERE id = ?',['reject',$id]);
            return redirect()->route('news.album',$news->news_id);
        }

        public function newsDelete($id)
        {
            $news = News::find($id);
            $news->delete();
            return redirect()->route('news.index');
        }

        public function newsalbumEdit(Request $request,$id)
        {
            // $this->validate($request, [

            // ]);
            $image = News_picture::where('id',$id)->get()->first();
            $i = 1;
            $path_pic = 'news';
            $data = $request->image;
            foreach ($data as $img) {
                $request->image = $img;
                $img_service = new ImageUploadController;
                $img_path = json_decode($img_service->uploadImageNews($request,$i,$path_pic)->content());
                $img_service->saveImgEdit($img_path->data,$id);
                $i++;
            }
            return redirect()->route('news.album',$image->news_id);
        }
}    // End News ------------------------------------------------------------

