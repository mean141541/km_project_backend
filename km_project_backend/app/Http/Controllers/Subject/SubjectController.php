<?php

namespace App\Http\Controllers\Subject;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subject;
use App\Department;

class SubjectController extends Controller
{
    public function subjectindex($id=null)
    {
        try{
            $subject = Subject::Where(['department_id'=>$id, 'status' => 'active'])->get();
            if($subject == null){
                return response(['message'=>'success','data'=>$subject],200);
            }else{
                return response(['message'=>'success','data'=>$subject],200);
            }
        }catch(\Exception $ex){
            return response([
                'message'=>'error',
                'data'=>$ex
            ],500);
        }
    }

    public function addSubject(Request $request)
    {
            $data = request()->validate(
                    [
                    'name'=>'required|string|max:30',
                    'pic_path'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                    ]
            );
            if(! (isset($request->name)) &&
            (gettype($request->name == 'string'))){
                return response([
                    'message'=>'Title does not exist',
                    'data'=>null
                ],400);
            }
            $access_validate = $request->bearerToken();
                if (! $access_validate){
                return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
            }else{

            try{

            $data = unserialize(base64_decode($access_validate));
            $subject = new Subject;
            $subject->department_id = $request->input('department_id');
            $subject->name = $request->input('name');
            $subject->created_at = date('Y-m-d H:i:s');
            $subject->updated_at = date('Y-m-d H:i:s');

            if($subject->name != null || $subject->status != null){

                if(!$request->pic_path == null){
                    $type_content_image = 'subject_pic';
                    $count_image_content = 1;
                    $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->pic_path->extension();
                    $request->pic_path->move(public_path('img/subject'), $imageName);
                    $file_path_to_database = '/img/subject/'.$imageName;
                    $subject->pic_path = $file_path_to_database;
                }else{
                    $subject->pic_path = '';
                }
                if ($data['permission'] == 'Admin'){
                    $subject->status = "active";
                }
                if ($data['permission'] == 'Leader'){
                    $subject->status = "active";
                }

                $department = Department::Where('id',$subject->department_id)->get()->first();
                if($department == null){
                    return response(['message'=>'department id not found','data'=>$subject],200);
                }else{
                    $subject->save();
                    return response(['message'=>'success','data'=>$subject],200);
                }

            }else{
                return response(['message'=>'subject is null'],422);
            }

        }catch(\Exception $ex){
            return response([
                'message'=>'service is not available',
                'data'=>null
            ],500);
        }
    }
    }

    public function editsubject(Request $request, $id=null)
    {
            $data = request()->validate(
                [
                'name'=>'required|string|max:30',
                'pic_path'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                ]
            );
            if(! (isset($request->name)) &&
            (gettype($request->name == 'string'))){
                return response([
                    'message'=>'Title does not exist',
                    'data'=>null
                ],400);
            }
            $access_validate = $request->bearerToken();
                if (! $access_validate){
                return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
            }else{

        try{

            $data = unserialize(base64_decode($access_validate));
            $subject = Subject::Where('id',$id)->get()->first();
            $subject->department_id = $request->input('department_id');
            $subject->name = $request->input('name');
            $subject->created_at = date('Y-m-d H:i:s');
            $subject->updated_at = date('Y-m-d H:i:s');

            if($subject->name != null || $subject->status != null){
                    if(!$request->pic_path == null){
                        $type_content_image = 'subject_pic';
                        $count_image_content = 1;
                        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->pic_path->extension();
                        $request->pic_path->move(public_path('img/subject'), $imageName);
                        $file_path_to_database = '/img/subject/'.$imageName;
                        $subject->pic_path = $file_path_to_database;
                    }else{
                        $subject->pic_path = '';
                    }

                    if ($data['permission'] == 'Admin'){
                        $subject->status = $request->status;
                    }
                    if ($data['permission'] == 'Leader'){
                        $subject->status = $request->status;
                    }

                    $department = Department::Where('id',$subject->department_id)->get()->first();
                    if($department == null){
                        return response(['message'=>'department id not found','data'=>$subject],200);
                    }else{
                        $subject->save();
                        return response(['message'=>'success','data'=>$subject],200);
                    }
            }else{
                return response(['message'=>'subject is null','data'=>$subject],200);
            }
        }catch(\Exception $ex){
            return response([
                'message'=>'service is not available',
                'data'=>null
            ],500);
        }
    }
    }


    public function deletesubject($id=null)
    {
        try{
            if($id == null){
                return response(['message'=>'success','data'=>$subject],200);
            }else{
                $subject = Subject::Where('id' ,$id)->get()->first();
                $subject->status = 'reject';
                $subject->save();
                return response(['message'=>'success','data'=>$subject],200);
            }
        }
        catch(\Exception $ex)
        {
            return response([
                'message'=>'service is not available',
                'data'=>null
            ],500);
        }
    }
}
