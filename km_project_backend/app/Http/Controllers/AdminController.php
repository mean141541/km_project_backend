<?php

namespace App\Http\Controllers;

use App\Admin;
use App\News;
use App\Http\Controllers\news\NewsController;
use Illuminate\Http\Request;
use App\Subject;
use App\Sub_subject;
use App\Department;
use App\Http\Controllers\Department\DepartmentController;
use App\User;
use App\Http\Controllers\user\UsersController;
use App\Content;
use App\Http\Controllers\content\ContentController;
use App\Content_picture;
use App\Content_type;
use DB;
use App\Courses;
use App\Http\Controllers\courses\CoursesController;
use App\Course;
use App\Http\Controllers\news\ImageUploadController as NewsImageUploadController;
use App\News_type;
use App\Http\Controllers\content\ContenBackgroudController;
use App\Http\Controllers\content\Content_v2_Controller;
use App\News_picture;
use App\Http\Requests\SubsubjectRequest;
use App\Config;

class AdminController extends Controller
{
    public function index(Admin $admin)
    {
        return view('Admin.adminIndex');
    }

    // Knowledge -------------------------------------------------------
    public function department(Admin $admin)
    {
        // $department = Department::all();
        $dept = DB::select('SELECT * FROM department WHERE status != "delete"');
        return view('Admin.Knowledge.Department.department',['dept'=>$dept]);

    }

    public function deptCreate()
    {
        return view('Admin.Knowledge.Department.create');
    }

    public function deptInsert(Request $request)
    {
        $this->validate($request, [

            'deptName' => 'required|string|max:30',
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        $type_content_image = 'department_pic';
        $count_image_content = 1;
        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
        $request->image->move(public_path('img/department'), $imageName);
        $file_path_to_database = '/img/department/'.$imageName;

        $name = $request->input('deptName');
        $pic = $file_path_to_database ;
        $status = $request->input('deptStatus');

        $dept = new Department();
        $dept->department_name = $name;
        $dept->pic = $pic;
        $dept->status = $status;
        $dept->save();

        return redirect()->route('dept.index');

    }

    public function deptEdit($id)
    {
        $dept = DB::select('SELECT * FROM department WHERE id = ?',[$id]);
        return view('Admin.Knowledge.Department.edit',['dept'=>$dept]);
    }

    public function deptUpdate(Request $request,$id)
    {
        $this->validate($request, [

            'deptName' => 'required|string|max:30',
        ]);
        $dept = Department::find($id);

        if($request->image != null){
            $type_content_image = 'department_pic';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
            $request->image->move(public_path('img/department'), $imageName);
            $file_path_to_database = '/img/department/'.$imageName;
            $dept->pic = $file_path_to_database;
        }
        $dept->department_name = $request->input('deptName');
        $dept->status = $request->input('deptStatus');
        $dept->save();

        return redirect()->route('dept.index');
    }

    public function deptReject($id)
    {
        DB::update('UPDATE department SET status = ? WHERE id = ?',['reject',$id]);
        return redirect()->route('dept.index');
    }

    public function deptDelete($id)
    {
        $department = Department::find($id);
        $department ->delete();
        return redirect()->route('dept.index');
    }

    public function subject(Admin $admin,$id)
    {
        $subject = DB::select('SELECT s.*, d.id AS deptID
                                FROM subject s, department d
                                WHERE s.department_id = d.id
                                AND d.id = ?', [$id]);


        return view('Admin.Knowledge.Subject.subject',['subject'=>$subject]);
    }

    public function subjectCreate(Admin $admin,$id)
    {
        $subject = DB::select('SELECT s.*, d.id AS deptID, d.department_name AS deptName
                                FROM subject s, department d
                                WHERE s.department_id = d.id
                                AND d.id = ?', [$id]);

        return view('Admin.Knowledge.Subject.create',['subject'=>$subject]);
    }

    public function subjectInsert(Request $request)
    {
        $this->validate($request, [

            'subjectName' => 'required|string|max:30',
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        $type_content_image = 'subject_pic';
        $count_image_content = 1;
        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
        $request->image->move(public_path('img/subject'), $imageName);
        $file_path_to_database = '/img/subject/'.$imageName;

        $deptId = $request->input('subjectDept');
        $name = $request->input('subjectName');
        $pic = $file_path_to_database ;
        $status = $request->input('subjectStatus');

        $subject = new Subject();
        $subject->department_id = $deptId;
        $subject->name = $name;
        $subject->pic_path = $pic;
        $subject->status = $status;
        $subject->save();

        return redirect()->route('subject.index',['id' => $deptId]);

    }

    public function subjectEdit($id)
    {
        $dept = DB::select('SELECT * FROM department');
        $subject = DB::select('SELECT s.*, d.id as deptID, d.department_name AS deptName
                                FROM subject s, department d
                                WHERE s.department_id = d.id
                                AND s.id = ?', [$id]);

        return view('Admin.Knowledge.Subject.edit',['subject'=>$subject],['dept'=>$dept]);
    }

    public function subjectUpdate(Request $request,$id)
    {
        $this->validate($request, [

            'subjectName' => 'required|string|max:30',
        ]);
        $subject = Subject::find($id);

        if($request->image != null){
            $type_content_image = 'subject_pic';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
            $request->image->move(public_path('img/subject'), $imageName);
            $file_path_to_database = '/img/subject/'.$imageName;
            $subject->pic_path = $file_path_to_database;
        }
        $deptId = $request->input('subjectDept');

        $subject->department_id = $deptId;
        $subject->name = $request->input('subjectName');
        $subject->status = $request->input('subjectStatus');
        $subject->save();

        return redirect()->route('subject.index',['id'=>$deptId]);
    }

    public function subjectReject($id)
    {
        $subject = DB::update('UPDATE subject SET status = ? WHERE id = ?',['reject',$id]);
        $dept = DB::select('SELECT d.id
                            FROM department d, subject s
                            WHERE d.id = s.department_id
                            AND s.id = ?', [$id]);
        // {{dd($dept);}}
        return redirect()->route('subject.index',['id'=>$dept[0]->id]);
    }

    public function subjectDelete($id)
    {
        
        $subject = Subject::find($id);
        $subid = $subject["department_id"];
        //dd($subid);
        $subject ->delete($id);
        // $dept = DB::select('SELECT d.id
        //                     FROM department d, subject s
        //                     WHERE d.id = s.department_id
        //                     AND s.id = ? ',[$subid]);
       
        return redirect()->route('subject.index',['id'=>$subid]);
    }

    public function sub(Admin $admin,$id)
    {
        $subject = DB::select('SELECT s.*, d.id AS deptID
                                FROM subject s, department d
                                WHERE s.department_id = d.id
                                AND s.id = ?', [$id]);

        $sub = DB::select('SELECT ss.*, s.id AS subjectID
                            FROM sub_subject ss, subject s
                            WHERE ss.subject_id = s.id
                            AND s.id = ?', [$id]);

        return view('Admin.Knowledge.Sub_subject.sub',['sub'=>$sub],['subject'=>$subject]);
    }

    public function subCreate(Admin $admin,$id)
    {
        
        $sub = DB::select('SELECT ss.*, s.id AS subjectID
                            FROM sub_subject ss, subject s
                            WHERE ss.subject_id = s.id
                            AND s.id = ?', [$id]);

        return view('Admin.Knowledge.Sub_subject.create',['sub'=>$sub]);
    }

    public function sub_subject_store(SubsubjectRequest $request)
    {
        $type_content_image = 'sub_subject_';
        $count_image_content = 1;
        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
        $request->image->move(public_path('img/myfolder'), $imageName);
        $file_path_to_database = '/img/myfolder/'.$imageName;
        $sub_subject = new Sub_subject([
            'title' => $request->title,
            'pic_path' => $file_path_to_database,
            'subject_id' => $request->subject_id,
            'detail' => $request->detail,
            'view' => '0',
            'status' => 'active',
            'tag' => $request->tag,
        ]);
        
        if(!$sub_subject->save())
        return redirect()->back();
        return redirect()->route('sub.index',['id'=> $request->subject_id]);
    }

    
    public function subEdit(Admin $admin,$id)
    {
        $sub_subject = Sub_subject::where('id', '=', $id)->first();
        // // $sub = DB::select('SELECT ss.*, s.id AS subjectID
        //                     -- FROM sub_subject ss, subject s
        //                     -- WHERE ss.subject_id = s.id
        //                     -- AND s.id = ?', [$id]);
       

        return view('Admin.Knowledge.Sub_subject.edit',['sub'=>$sub_subject]);
    }

    // public function sub_subject_Update(Request $request, $id)
    // {
    //     $sub_subject = Sub_subject::where($id);
    //     $type_content_image = 'sub_subject_';
    //     $count_image_content = 1;
    //     $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
    //     $request->image->move(public_path('img/myfolder'), $imageName);
    //     $file_path_to_database = '/img/myfolder/'.$imageName;
    //    ([
    //         'title' => $request->title,
    //         'pic_path' => $file_path_to_database,
    //         'subject_id' => $request->subject_id,
    //         'detail' => $request->detail,
    //         'view' => '0',
    //         'status' => 'active',
    //         'tag' => $request->tag,
    //     ]);
    //     // dd($sub_subject);
    //     $sub_subject->save();
    //     if(!$sub_subject->save())
    //     // dd($sub_subject);
    //     return redirect()->back();
    //     return redirect()->route('sub.index',['id'=> $request->subject_id]);
    // }

    // public function sub_subject_update(SubsubjectRequest $request, $id)
    // {
    //     $sub_subject = Sub_subject::where('id', '=', $id)->first();
    //         //    $type_content_image = 'sub_subject_';
    //         //    $count_image_content = 1;
    //         //    $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
    //         //    $request->image->move(public_path('img/myfolder'), $imageName);
    //         //    $file_path_to_database = '/img/myfolder/'.$imageName;
               
    //         // //    $sub_subject = Sub_subject::where
    //         // ([
    //         //         'title' => $request->title,
    //         //         'pic_path' => $file_path_to_database,
    //         //         'subject_id' => $request->subject_id,
    //         //         'detail' => $request->detail,
    //         //         'view' => '0',
    //         //         'status' => 'active',
    //         //         'tag' => $request->tag,
    //         //     ]);
    //             // dd($sub_subject);
    //             $sub_subject->update($request->all());
    //             // $sub_subject->save();
    //             dd($sub_subject);
    // //             if(!$sub_subject->save())
    // //             return redirect()->back();
    // //             return redirect()->route('sub.index',['id'=> $request->subject_id]);
    // }


    public function subDelete($id)
    {
        $sub = Sub_subject::find($id);
        //$sub_id = $sub_subject["subject_id"];
        // $subid = $sub_subject["subject_id"];
        //dd($subid);
        $sub ->delete();
       
        // return redirect()->route('sub.index',['id'=>$sub]);
        return redirect()->route('sub.index',['id'=>$subject_id]);
    }

    // End Knowledge ---------------------------------------------------


    //course ********************************************************************
    public function Courses ()
    {

        // $courses = Courses::all();
           $courses = DB::select ("SELECT * FROM courses ORDER BY id DESC");

        // dd($courses);

        return view('Admin.course.index' , compact('courses'));

    }

    public function  coursedetail(Request $request,$id){
        $courses = Courses::find($id);
        return view('Admin.course.detail',compact('courses'));

   }

   public function createcourseform(){
    return view('Admin.course.create');
}


    public function coursesInsert(Request $request){
            $request->user_id = 5;
            // dd($request->user_id);
            //  $courses = new CoursesController;
            //  $data = json_decode($courses->create($request)->content());
            // dd($data);

            $this->validate($request,[
                'title'=>'required',
                'detail'=>'required',
                'path_pic'=>'required|image|mimes:jpeg,png,jpg,svg|max:2048']);

            $type_courses_image = 'Courses_';
            $count_image_courses = 1;
            $imageName = $type_courses_image.date('YmdHis').'_'.$count_image_courses.'.'.$request->path_pic->extension();
            $request->path_pic->move(public_path('img/Courses'), $imageName);
            $file_path_to_database = '/img/Courses/'.$imageName;
            $courses = new Courses(
                    [
                        'title' => $request->get('title'),
                        'detail' => $request->get('detail'),
                        'path_pic' => $file_path_to_database,
                        'status' => 'active',
                        'user_id' => $request->user_id
                    ]
                    );
         $courses->save();
         return redirect()->route('course.index');

     }



     public function coursesedit(Request $request,$id)
     {
        $courses = Courses::find($id);

        return view('Admin.course.edit',compact('courses'));

     }


     public function coursesupdate(Request $request, $id)
     {

        $this->validate($request,[
            'title'=>'required',
            'detail'=>'required',
            'status'=>'required',
            //'user_id'=>'required'
            ]);

        $courses = Courses::find($id);
        //   dd($courses);
           if($request->path_pic != null){
               $type_content_image = 'Courses_';
               $count_image_content = 1;
               $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->path_pic->extension();
               $request->path_pic->move(public_path('img/Courses'), $imageName);
               $file_path_to_database = '/img/Courses/'.$imageName;
               $courses->path_pic = $file_path_to_database;
                }
            // dd($file_path_to_database);
            $courses->title = $request->get('title');
            // $courses->title = $request->get('user_id');
            $courses->detail = $request->get('detail');
            $courses->status = $request->get('status');
            // $courses->status = $request->input('status');

            $courses->save();

           return redirect()->route('course.index');

    }






    public function Coursesvideos()
    {
        $courses_vdo = User::all()->toArray();
        return view('Admin.course.video.video',compact('$courses_vdo'));


        // dd( $coursesvdo );
    }



   //course ********************************************************************

    // User
    public function user(Admin $admin)
    {
        $detailuser = User::all()->toArray();
        return view('Admin.User.index',compact('detailuser'));
    }

    public function userCreate()
    {
        return view('Admin.User.create');
    }

    public function userInsert(Request $request)
    {
        $this->validate($request, [

            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users|max:255',
            'password' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);
        $type_content_image = 'user_pic';
        $count_image_content = 1;
        $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
        $request->image->move(public_path('img/user'), $imageName);
        $file_path_to_database = '/img/user/'.$imageName;

        $name = $request->input('name');
        $lastname = $request->input('lastname');
        $avatar = $file_path_to_database ;
        $email = $request->input('email');
        $password = bcrypt($request->input('password'));
        $permission = $request->input('permission');
        // $data=array("name"=>$name,"lastname"=>$lastname,"avatar"=>$avatar,"email"=>$email,"password"=>$password,"permission"=>$permission);
        // DB::table('users')->insert($data);

        $user = new User();
        $user->name = $name;
        $user->lastname = $lastname;
        $user->avatar = $avatar;
        $user->email = $email;
        $user->password = $password;
        $user->permission = $permission;
        $user->save();

        return redirect()->route('user.index');

    }

    public function userEdit($id)
    {
        $user = DB::select('SELECT * FROM users WHERE id = ?',[$id]);
        return view('Admin.User.edit',['user'=>$user]);
    }

    public function userUpdate(Request $request,$id)
    {
        $this->validate($request, [

            'name' => 'required|string|max:255',
        ]);
        $user = User::find($id);

        if($request->image != null){
            $type_content_image = 'user_pic';
            $count_image_content = 1;
            $imageName = $type_content_image.date('YmdHis').'_'.$count_image_content.'.'.$request->image->extension();
            $request->image->move(public_path('img/user'), $imageName);
            $file_path_to_database = '/img/user/'.$imageName;
            $user->avatar = $file_path_to_database;
        }

        $user->name = $request->input('name');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->permission = $request->input('permission');
        $user->save();

        return redirect()->route('user.index');
    }

    public function userDelete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('user.index');
    }

    // End User

    // travel
    public function Travel(Admin $admin)
    {

        $Travel_service = new ContentController;
        $data = json_decode($Travel_service->indexAll()->content());
        $data = $data->data;
        // dd($data);

        return view('Admin.Travel.index',compact('data'));

    }

    public function TravelCreate()
    {
        // dd('555');
        $Travel_service = new Content_v2_Controller;
        $data = json_decode($Travel_service->content_type()->content());
        $data = $data->data;
        // dd($data);


//  dd($data);

        return view('Admin.Travel.create',compact('data'));
    }

    public function travelInsert(Request $request)
    {
        $this->validate($request, [

            'title' => 'required',
            'type_id' => 'required',
            'detail' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',

        ]);

        $title = $request->input('title');
        $type_id = $request->input('type_id');
        $detail = $request->input('detail');
        $status = 'active';

        $file_service = new FileController;
        $detail = $file_service->postSummernote($request);

        $travel = new Content();
        $travel->type_id = $type_id;
        $travel->title = $title;
        $travel->detail = $detail;
        $travel->status = $status;


        $travel->save();
        $i = 1;
        $path_pic="content";
        $data = $request->image;
        foreach ($data as $img) {
            $request->image = $img;
            $img_service = new ImageUploadController;
            $img_path = json_decode($img_service->uploadImage($request,$i,$path_pic)->content());
            $content_picture = new ContentController;
            $content_picture->saveImg($img_path->data,$travel->id);
            $i++;
        }

        return redirect()->route('Travel.index');

    }

    public function TravelEdit($id)
    {
        $content = new ContentController;
        $content2= new Content_v2_Controller;
        $type = json_decode($content2->content_type()->content())->data;
        $data = json_decode($content->clickedit($id)->content())->data;
    return view('Admin.Travel.edit',compact('data','type'));
    }

    public function TravelUpdate(Request $request)
    {
        $this->validate($request, [

            'title' => 'required',
            'type_id' => 'required',
            'detail' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',

        ]);

        $title = $request->input('title');
        $type_id = $request->input('type_id');
        $detail = $request->input('detail');
        $status = 'active';

        $file_service = new FileController;
        $detail = $file_service->postSummernote($request);

        $travel = new Content();
        $travel->type_id = $type_id;
        $travel->title = $title;
        $travel->detail = $detail;
        $travel->status = $status;


        $travel->save();
        $i = 1;
        $path_pic="content";
        $data = $request->image;
        foreach ($data as $img) {
            $request->image = $img;
            $img_service = new ImageUploadController;
            $img_path = json_decode($img_service->uploadImage($request,$i,$path_pic)->content());
            $content_picture = new ContentController;
            $content_picture->saveImg($img_path->data,$travel->id);
            $i++;
        }

        return view('Admin.Travel.update',compact('data','type'));

    }


    public function TravelAlbum($id)
    {
        $data = Content_picture::where('content_id',$id)->where('status','active')->get()->all();

        return view('Admin.Travel.album',compact('data'));

    }


    public function editimage(Request $request,$id = null)
    {
    $content = Content_picture::where('path_pic',$id)->get()->first();
    $content->path_pic = $request->path_pic;
    dd($content);
    $content->save();
    // dd($content);
        //  return response(['message'=>'success','data'=>$request],200);
        return redirect()->route('Travel.index');
    }

    //=======================end content================================


    //configweb
    // public function config(){


    // }





}
