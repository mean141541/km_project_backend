<?php

namespace App\Http\Controllers\banned_word;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Banned_word;

class BannedwordController extends Controller
{
    public function get_all_banned_word()
    {
        $banned = DB::select("SELECT id AS id, word AS word, status FROM banned_word WHERE status='Active'");
        if(!$banned){
            return response([
                'message'=>'success',
                'data'=>'banned word not found'
            ],200);
        }
        return response([
            'message'=>'success',
            'data'=>$banned
        ],200);
    }
    public function search_banned_word($word)
    {
        $sql = "SELECT distinct id AS id, word AS word, status FROM banned_word WHERE status='Active' AND word LIKE '%".$word."%'";
        $data = DB::select($sql);
        return response([
            'message'=>'success',
            'data'=>$data
        ],200);
    }
    public function add_banned_word(Request $request)
    {
        $access_validate = $request->bearerToken();
        if (! $access_validate){
            return response()->json([
                'message' => 'Unauthorization',
                'data'=>$access_validate
            ],401);
        }else{
            try {
                $data = unserialize(base64_decode($access_validate));
            }catch (\Exception $th){
                return response()->json([
                    'message' => 'Unauthorization',
                    'data'=>$access_validate
                ],401);
            }
                $request->validate([
                    'word'=> 'required|string|max:200'
                ]);
                $check_duplicate = DB::select("SELECT * FROM banned_word WHERE word = ? AND status = 'Active'",[$request->word]);
                if($check_duplicate){
                    return response([
                        'message'=>'error',
                        'data'=>'this word is already exist word "'.$request->word.'"'
                    ],412);
                }
                $banned = new Banned_word();
                $banned->word = $request->word;
                $banned->status = 'Active';
                $banned->save();
                return response([
                    'message'=>'success',
                    'data'=>[
                        'word'=>$request->word
                    ]
                ],201);
        }
    }
    public function edit_banned($id,Request $request)
    {
        $request->validate([
            'word'=>'required|string|max:200'
        ]);
        if(!$id){
            return response()->json([
                'message' => 'error',
                'data'=>'id is required'
            ],412);
        }
        $validate = Banned_word::where('id',$id);
        if(!$validate){
            return response()->json([
                'message' => 'error',
                'data'=>'this is id does not exist'
            ],412);
        }
        $check_duplicate = DB::select("SELECT * FROM banned_word WHERE word = ? AND status = 'Active'",[$request->word]);
        if($check_duplicate){
            return response([
                'message'=>'error',
                'data'=>'this word is already exist word "'.$request->word.'"'
            ],412);
        }
        $banned = Banned_word::where('id',$id)->get()->first();
        $banned->word = $request->word;
        $banned->save();
        return response([
            'message'=>'success',
            'data'=>[
                'id'=>$id
            ]
        ],200);
    }
    public function delete_banned($id)
    {
        if(!$id){
            return response()->json([
                'message' => 'error',
                'data'=>'id is required'
            ],412);
        }
        $validate = Banned_word::where('id',$id)->get()->first();
        if(!$validate){
            return response()->json([
                'message' => 'error',
                'data'=>'this is id does not exist'
            ],412);
        }
        $banned = DB::update("UPDATE banned_word SET status = 'Reject' WHERE id = ?",[$id]);
        return response([
            'message'=>'success',
            'data'=>[
                'id'=>$id
            ]
        ],200);
    }
}
