<?php

namespace App\Http\Controllers\covid;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CovidController extends Controller
{
    public function today()
    {
        try{
            $url = 'https://covid19.th-stat.com/api/open/today';
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
            $response = curl_exec ($ch);
            $res = json_decode($response);
            return response(
                ['message'=>'success',
                'data'=>$res
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }
    }

    public function from_year_begin()
    {
        try{
            $url = 'https://covid19.th-stat.com/api/open/timeline';
      
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
            $response = curl_exec ($ch);
            $res = json_decode($response);
            return response([
                'message'=>'success',
                'data'=>$res
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }
    }

    public function case_by_case()
    {
        try{
            $url = 'https://covid19.th-stat.com/api/open/cases';
      
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
            $response = curl_exec ($ch);
            $res = json_decode($response);
            return response([
                'message'=>'success',
                'data'=>$res
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }
    }
    public function thai_and_global()
    {
        try{
            $url = 'https://covid19.th-stat.com/api/open/cases/sum';
      
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
            $response = curl_exec ($ch);
            $res = json_decode($response);
            return response([
                'message'=>'success',
                'data'=>$res
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }
    }
    public function redzone()
    {
        try{
            $url = 'https://covid19.th-stat.com/api/open/area';
      
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
            $response = curl_exec ($ch);
            $res = json_decode($response);
            return response([
                'message'=>'success',
                'data'=>$res
            ],200);
        } catch (\Exception $th){
            return response([
                'message'=>'error',
                'data'=>'service is not available'
            ],500);
        }
    }
}
