<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'courses';
    protected $fillable=['title','detail','path_pic','status','user_id'];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function courses_vdo()
    {
        return $this->hasMany(Courses_vdo::class);
    }
    public function courses_comment()
    {
        return $this->hasMany(Courses_comment::class);
    }
}
