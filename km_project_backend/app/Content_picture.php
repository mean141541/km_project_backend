<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content_picture extends Model
{
    protected $table = 'content_picture';
    protected $fillable = ['path_pic'];
    public function content()
    {
        return $this->belongsTo(Content::class, 'content_id', 'Content_picture');
    }
}
