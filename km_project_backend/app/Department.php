<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department';
    protected $fillable = ['department_name','pic'];

    public function subject()
    {
        return $this->hasMany(Subject::class);
    }
}
