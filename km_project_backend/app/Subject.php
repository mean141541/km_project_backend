<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subject';

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function sub_subject()
    {
        return $this->hasMany(Sub_subject::class);
    }
}
