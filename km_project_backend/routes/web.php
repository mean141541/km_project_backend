<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'AdminController@index');
Route::post('/test','test\TestController@index');
Route::get('/testing', function () {
    return view('testing.test');
});

Route::get('Admin', 'AdminController@index')->name('admin.index');

// Route Knowledge Department
Route::get('Admin/Knowledge/Department', 'AdminController@department')->name('dept.index');
Route::get('Admin/Knowledge/Department/Create', 'AdminController@deptCreate')->name('dept.create');
Route::post('Admin/Knowledge/Department/Insert', 'AdminController@deptInsert')->name('dept.insert');
Route::get('Admin/Knowledge/Department/{id}/edit', 'AdminController@deptEdit')->name('dept.edit');
Route::post('Admin/Knowledge/Department/{id}/update', 'AdminController@deptUpdate')->name('dept.update');
Route::get('Admin/Knowledge/Department/{id}/reject', 'AdminController@deptReject')->name('dept.reject');
Route::get('Admin/Knowledge/Department/{id}/delete', 'AdminController@deptDelete')->name('dept.delete');

// Route Knowledge Subject
Route::get('Admin/Knowledge/Subject/{id}/index', 'AdminController@subject')->name('subject.index');
Route::get('Admin/Knowledge/Subject/{id}/Create', 'AdminController@subjectCreate')->name('subject.create');
Route::post('Admin/Knowledge/Subject/Insert', 'AdminController@subjectInsert')->name('subject.insert');
Route::get('Admin/Knowledge/Subject/{id}/edit', 'AdminController@subjectEdit')->name('subject.edit');
Route::post('Admin/Knowledge/Subject/{id}/update', 'AdminController@subjectUpdate')->name('subject.update');
Route::get('Admin/Knowledge/Subject/{id}/reject', 'AdminController@subjectReject')->name('subject.reject');
Route::get('Admin/Knowledge/Subject/{id}/delete/', 'AdminController@subjectDelete')->name('subject.delete');

// Route Knowledge Sub_Subject
Route::get('Admin/Knowledge/Subject/Sub/{id}/index', 'AdminController@sub')->name('sub.index');
Route::get('Admin/Knowledge/Subject/Sub/{id}/Create', 'AdminController@subCreate')->name('sub.create');
Route::get('Admin/Knowledge/Subject/Sub/{id}/Edit', 'AdminController@subEdit')->name('sub.edit');
Route::post('Admin/Knowledge/Subject/Sub/Store', 'AdminController@sub_subject_store')->name('sub.store');
Route::post('Admin/Knowledge/Subject/Sub/Update', 'AdminController@sub_subject_Update')->name('sub.update');
Route::get('Admin/Knowledge/Subject/Sub/{id}/delete', 'AdminController@subDelete')->name('sub.delete');


// Route News
Route::get('Admin/news', 'admin\adminNewsController@news')->name('news.index');
Route::get('Admin/news/{id}/detail', 'admin\adminNewsController@newsDetail')->name('news.detail');
Route::post('Admin/news/{id}/update', 'admin\adminNewsController@newsUpdate')->name('news.update');
Route::get('Admin/news/create', 'admin\adminNewsController@newsCreate')->name('news.create');
Route::post('Admin/news/insert', 'admin\adminNewsController@newsInsert')->name('news.insert');
Route::get('Admin/news/{id}/delete', 'admin\adminNewsController@newsDelete')->name('news.delete');

// Route News type
Route::get('Admin/news_type', 'admin\adminNewsController@newsType')->name('news.type');
Route::get('Admin/news_type/{id}/detail', 'admin\adminNewsController@newstypeDetail')->name('news_type.detail');
Route::post('Admin/news_type/{id}/update', 'admin\adminNewsController@newstypeUpdate')->name('news_type.update');
Route::get('Admin/news_type/create', 'admin\adminNewsController@newstypeCreate')->name('news_type.create');
Route::post('Admin/news_type/insert', 'admin\adminNewsController@newstypeInsert')->name('news_type.insert');

// Route News Album
Route::get('Admin/news_album/detail/{id}', 'admin\adminNewsController@newsAlbum_detail')->name('news.album.detail');
Route::get('Admin/news_album/{id}/create', 'admin\adminNewsController@newsAlbum_create')->name('news.album.create');
Route::post('Admin/news_album/{id}/update', 'admin\adminNewsController@newsAlbum_update')->name('news.album.update');
Route::get('Admin/news_album/{id}/detail', 'admin\adminNewsController@newsAlbum')->name('news.album');
Route::post('Admin/news_album/{id}/insert', 'admin\adminNewsController@newsalbumInsert')->name('news_album.insert');
Route::post('Admin/news_album/{id}/edit', 'admin\adminNewsController@newsalbumEdit')->name('news_album.edit');
Route::get('Admin/news_album/{id}/delete', 'admin\adminNewsController@newsalbumDelete')->name('news_album.delete');


// Route User
Route::get('Admin/User', 'AdminController@user')->name('user.index');
Route::get('Admin/User/Create', 'AdminController@userCreate')->name('user.create');
Route::post('Admin/User/Insert', 'AdminController@userInsert')->name('user.insert');
Route::get('Admin/User/edit/{id}', 'AdminController@userEdit')->name('user.edit');
Route::post('Admin/User/update/{id}', 'AdminController@userUpdate')->name('user.update');
Route::get('Admin/User/delete/{id}', 'AdminController@userDelete')->name('user.delete');

// Route::get('test', 'FileController@getSummernote')->name('summernote.post');
// Route::get('summernote',array('as'=>'summernote.get','uses'=>'FileController@getSummernote'));
// Route::post('summernote',array('as'=>'summernote.post','uses'=>'FileController@postSummernote'));
Route::get('summernote','FileController@getSummernote')->name('summernote.get');
Route::post('summernote','FileController@postSummernote')->name('summernote.post');

// Route course
Route::get('Admin/course', 'AdminController@Courses')->name('course.index');
Route::get('Admin/course/create', 'AdminController@createcourseform')->name('course.create');
Route::post('Admin/course/Insert', 'AdminController@coursesInsert')->name('course.insert');
Route::get('Admin/course/{id}/edit','AdminController@coursesedit')->name('course.edit');
Route::put('Admin/course/{id}/update','AdminController@coursesupdate')->name('course.update');
Route::get('Admin/course/{id}/detail', 'AdminController@coursedetail')->name('course.detail');


Route::get('Admin/course/vedio', 'AdminController@Coursesvideos')->name('course.vedio');

// Route course


Route::get('summernote',array('as'=>'summernote.get','uses'=>'FileController@getSummernote'));
Route::post('summernote',array('as'=>'summernote.post','uses'=>'FileController@postSummernote'));


// Route Travel

Route::get('Admin/Travel', 'AdminController@Travel')->name('Travel.index');
Route::get('Admin/Travel/create', 'AdminController@TravelCreate')->name('Travel.create');
Route::post('Admin/Travel/insert', 'AdminController@TravelInsert')->name('Travel.insert');
Route::get('Admin/Travel/edit/{id}', 'AdminController@TravelEdit')->name('Travel.edit');
Route::get('Admin/Travel/update/{id}', 'AdminController@TravelUpdate')->name('Travel.Update');
// Route::put('/content/update/{id}', 'AdminController@updateedit')->name('Travel.edit2');;

Route::get('/Admin/rdr/{token}','redirect_from_front\Redirect_from_front_Controller@index')->name('redierct_from_front');
Route::get('/rdr/{token}','redirect_from_front\Redirect_from_front_Controller@index')->name('redierct_from_front');

Route::get('Admin/Travel/{id}', 'AdminController@TravelAlbum')->name('Travel.album');
Route::post('/content/image/uplode/{id}', 'AdminController@editimage')->name('Travel.edit3');


// Route Config
// Route::get('Admin/Config', 'AdminController@config')->name('config.index');
// Route::get('Admin/Config/Create', 'AdminController@configCreate')->name('config.create');
// Route::post('Admin/Config/Insert', 'AdminController@configInsert')->name('config.insert');
// Route::get('Admin/Config/edit/{id}', 'AdminController@configEdit')->name('config.edit');
// Route::post('Admin/Config/update/{id}', 'AdminController@configUpdate')->name('config.update');
// Route::get('Admin/Config/delete/{id}', 'AdminController@configDelete')->name('config.delete');

Route::get('Admin/Config', 'webconfig\ConfigController@Config')->name('config.index');
Route::post('Admin/Config/Create', 'webconfig\ConfigController@add_contract')->name('config.create');
Route::get('Admin/Config/edit/{config}', 'webconfig\ConfigController@selectConfig')->name('config.edit');
Route::post('Admin/Config/update/{config}', 'webconfig\ConfigController@configUpdate')->name('config.update');
//Route::get('Admin/Config/edit/{id}', 'webconfig\EditConfigController@edit_photo')->name('config.editphoto');
Route::post('Admin/Config/Insert', 'webconfig\ConfigController@configInsert')->name('config.insert');
Route::get('Admin/Config/delete/{id}', 'webconfig\ConfigController@configDelete')->name('config.delete');

