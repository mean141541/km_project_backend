<?php

// use App\Http\Controllers\DepartmentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('department','DepartmentController@departmentindex' );

// get department api
Route::get('depart','Department\DepartmentController@index');
Route::get('depart/{id}','Department\DepartmentController@depart');
// Route::post('departcreate','Department\DepartmentController@create');
// Route::post('departupdate/{id}','Department\DepartmentController@update');
// Route::delete('delete/{id}','Department\DepartmentController@deletedepartment');
Route::post('departcreate','Department\DepartmentController@create')->middleware('admin');
Route::post('departupdate/{id}','Department\DepartmentController@update')->middleware('admin');
Route::delete('delete/{id}','Department\DepartmentController@deletedepartment')->middleware('admin');
Route::put('departapprove/{id}','Department\DepartmentController@approveDepartment');

Route::get('subject','Department\DepartmentController@subject');
// Route::get('subjectid/{id}','Department\DepartmentController@subjectid');

//subject
Route::get('subject/{id}', 'Subject\SubjectController@subjectindex');
// Route::post('subject/add', 'Subject\SubjectController@addSubject');
// Route::post('subject/edit/{id}', 'Subject\SubjectController@editsubject');
// Route::delete('subject/delete/{id}', 'Subject\SubjectController@deletesubject');
Route::post('subject/add', 'Subject\SubjectController@addSubject')->middleware('admin');
Route::post('subject/edit/{id}', 'Subject\SubjectController@editsubject')->middleware('admin');
Route::delete('subject/delete/{id}', 'Subject\SubjectController@deletesubject')->middleware('admin');

//subjectUX
// Route::get('subjectux', 'Subject\SubjectUXController@indexux');
// Route::get('subjectux/{id}', 'Subject\SubjectUXController@subjectux');
// Route::post('subjectux/add', 'Subject\SubjectUXController@addsubjectUX');
// Route::post('subjectux/edit/{id}', 'Subject\SubjectUXController@addsubjectUX');

// sub_subject api
Route::get('subsubject/{id}', 'Subsubject\SubsubjectController@subsubjectindex');
Route::get('subsubject/show/{id}', 'Subsubject\SubsubjectController@showsubsubject');
Route::post('subsubject/edit/{id}', 'Subsubject\SubsubjectController@editsubsubject')->middleware('admin');
Route::post('subsubject/add', 'Subsubject\SubsubjectController@addSub_subject')->name('sub_subject_add');
Route::delete('subsubject/delete/{id}', 'Subsubject\SubsubjectController@deletesubsubject')->middleware('admin');
// Route::post('subsubject/edit/{id}', 'Subsubject\SubsubjectController@editsubsubject');
// Route::post('subsubject/add', 'Subsubject\SubsubjectController@addSub_subject');
// Route::delete('subsubject/delete/{id}', 'Subsubject\SubsubjectController@deletesubsubject');
Route::get('/subsubject/landing_page/count_with_view', 'news\LandingpageNewsController@news_count_with_view');
Route::get('/subsubject/landing_page/news_count_with_upload_date', 'news\LandingpageNewsController@news_count_with_date_upload');


Route::get('/content/index', 'content\ContentController@create');

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('avatar', 'AuthController@upload_avatar');

});

Route::get('auth/logout', 'redirect_from_front\Redirect_from_front_Controller@logout')->name('logout');



// คอนเท้น
Route::get('/content/index', 'content\ContentController@index');
Route::get('/v2/content/index', 'content\Content_v2_Controller@index');
Route::get('/v2/content/index/{id}', 'content\Content_v2_Controller@index_by_id');
// Route::get('/content/travel', 'content\ContentController@travel');
Route::get('/content/clickedit/{id}', 'content\ContentController@clickedit');
Route::put('/content/update/{id}', 'content\ContentController@updateedit');
Route::post('/content/inserts', 'content\ContentController@inserts');
Route::delete('/content/delete/{id}', 'content\ContentController@delete');
Route::put('/content/updaedit/{id}', 'content\ContentController@updateedit');
Route::put('/content/updatestatus/{id}', 'content\ContentController@statusedit');


// คอนเท้นรูปภาพ
Route::post('/content/image/edit/{id}', 'content\ContentController@imageEdit');
Route::delete('/content/image/delete/{id}', 'content\ContentController@imageDelete');
Route::post('/content/image/uplode/{id}', 'content\ContentController@imageUpload');
Route::put('/content/addstatusImage/{id}', 'content\ContentController@addstatusImage')->middleware('admin');

Route::get('/content_backgroud', 'content\ContenBackgroudController@index');
Route::get('/content_backgroud/{id}', 'content\ContenBackgroudController@index_by_type');
Route::post('/content_backgroud/add', 'content\ContenBackgroudController@add')->middleware('admin');
Route::post('/content_backgroud/edit', 'content\ContenBackgroudController@edit')->middleware('admin');
Route::delete('/content_backgroud/delete/{id}', 'content\ContenBackgroudController@delete')->middleware('admin');
Route::put('/content/statusbg/{id}', 'content\ContenBackgroudController@statusbg')->middleware('admin');


//contenttype
Route::post('/content/insertstype', 'content\ContentController@insertstype');
Route::delete('/content/deletetype/{id}', 'content\ContentController@deletetype');
Route::put('/content/updatestatustype/{id}', 'content\ContentController@statusedittype');
Route::put('/content/edittype/{id}', 'content\ContentController@edittype');






//news
Route::get('/news/indexAll', 'news\NewsController@indexAll');  //news+activity

Route::get('/news/index', 'news\NewsController@indexNews');     //news
Route::post('/news/insert', 'news\NewsController@insertNews');
Route::put('/news/edit/{id}', 'news\NewsController@editNews');
Route::put('/news/delete/{id}', 'news\NewsController@deleteNews');
Route::get('/news/index/{id}', 'news\NewsController@index');        //news or actuvity By ID
Route::put('/news/approve/{id}', 'news\NewsController@approveNews')->middleware('admin');

//news Activity
Route::get('/news/activity/index', 'news\NewsController@indexAll_activity');

//news Type
Route::get('/news/type', 'news\NewstypeController@indexType');
Route::get('/news/index/type/{id}', 'news\NewstypeController@indexTypebyID');
Route::post('/news/insert/type', 'news\NewstypeController@insertType')->middleware('admin');
Route::put('/news/edit/type/{id}', 'news\NewstypeController@editType')->middleware('admin');
Route::put('/news/delete/type/{id}', 'news\NewstypeController@deleteType')->middleware('admin');
//news Image
Route::post('/news/image/edit/{id}', 'news\ImageUploadController@imageEdit');
Route::put('/news/image/delete/{id}', 'news\ImageUploadController@imageDelete');
Route::post('/news/image/uplode/{id}', 'news\ImageUploadController@imageUpload');
Route::put('/news/image/approve/{id}', 'news\ImageUploadController@approveImage')->middleware('admin');
Route::get('/news/image/indexAll', 'news\ImageUploadController@imageIndexAll');
Route::get('/news/image/index/{id}', 'news\ImageUploadController@imageIndex');
Route::get('/news/lastestnews', 'news\LastestNewsController@ActivityNew');

//log
Route::get('/news/log/index', 'news\NewsController@indexLog');
Route::get('/news/type/log/index', 'news\NewstypeController@indexLog');
Route::get('/news/picture/log/index', 'news\ImageUploadController@indexLog');

//admin
Route::put('/admin/permissions', 'user\PermissionController@edit_permission')->middleware('admin');
Route::delete('/admin/users', 'user\UsersController@delete_user')->middleware('admin');
Route::get('/admin/users', 'user\UsersController@get_all_user')->middleware('admin');
Route::put('/admin/edit_email', 'user\UsersController@edit_email')->middleware('admin');
Route::put('/admin/edit_name', 'user\UsersController@edit_name')->middleware('admin');
Route::put('/change_password', 'user\UsersController@edit_password')->middleware('logging');


Route::post('imageUploadPost', 'ImageUploadController@imageUploadPost')->name('imageuploadpost');
Route::get('/content/travel', 'content\ContentController@travel');
Route::get('/content/clickedit/{id}', 'content\ContentController@clickedit');
Route::put('/content/update/{id}', 'content\ContentController@updateedit');
Route::post('/content/inserts', 'content\ContentController@inserts');
Route::post('imageupload', 'ImageUploadController@imageUploadPost')->name('imageuploadpost');


// contract
Route::get('/contact', 'webconfig\ConfigController@selectConfig');
Route::put('/admin/edit_contact', 'webconfig\EditConfigController@edit')->middleware('admin');
Route::post('/admin/edit_contact_image', 'webconfig\EditConfigController@edit_photo')->middleware('admin');
Route::post('/admin/add_contact', 'webconfig\ConfigController@add_contract')->middleware('admin');

//covid
Route::get('/covid/today', 'covid\CovidController@today');
Route::get('/covid/from_year_begin', 'covid\CovidController@from_year_begin');
Route::get('/covid/case_by_case', 'covid\CovidController@case_by_case');
Route::get('/covid/thai_and_global', 'covid\CovidController@thai_and_global');
Route::get('/covid/redzone', 'covid\CovidController@redzone');

//comment
Route::post('/comment/add', 'comment\AddcommentController@add')->middleware('logging');
Route::get('/comment/get/{id}', 'comment\CommentController@getComment');
Route::get('/comment/get', 'comment\CommentController@all_comment')->middleware('leader');
Route::delete('/comment/delete/{id}', 'comment\AddcommentController@delete_comment')->middleware('admin');

//banned_word
Route::get('/banned_word', 'banned_word\BannedwordController@get_all_banned_word');
Route::get('/banned_word/{word}/get', 'banned_word\BannedwordController@search_banned_word');
Route::post('/banned_word_add', 'banned_word\BannedwordController@add_banned_word')->middleware('leader');
Route::put('/banned_word/{id}/put', 'banned_word\BannedwordController@edit_banned')->middleware('admin');
Route::delete('/banned_word/{id}/delete', 'banned_word\BannedwordController@delete_banned')->middleware('admin');

//Courses_Ta
Route::post('/courses/create', 'courses\CoursesController@create')->middleware('admin');
Route::get('/courses', 'courses\CoursesController@Show');
Route::get('/courses/{id}', 'courses\CoursesController@getId');
Route::put('/courses/update/{id}', 'courses\CoursesController@update')->middleware('admin');
Route::put('/courses/delete/{id}', 'courses\CoursesController@delete')->middleware('admin');
Route::post('/courses/pic/{id}', 'courses\CoursesImgController@imageEdit')->middleware('admin');

// Courses Vdo
// เรียกเฉพาะ course_vdo id
Route::get('course_vdo','course_vdo\CourseVdoController@index');
// รวมคอร์สวิดิโอ
Route::get('course_vdo/show/{course_id}','course_vdo\CourseVdoController@show');
Route::post('course_vdo/add','course_vdo\CourseVdoController@create')->middleware('admin');
// เรียกเฉพาะ course_vdo id
Route::get('course_vdo/{id}', 'course_vdo\CourseVdoController@getId');
Route::put('course_vdo/update/{id}','course_vdo\CourseVdoController@update')->middleware('admin');
Route::put('course_vdo/delete/{id}','course_vdo\CourseVdoController@delete')->middleware('admin');
//couses comment
Route::get('/couses/comment/{id}/get', 'couses\CommentCouseController@get_comment');
Route::post('/couses/comment/add', 'couses\CommentCouseController@add_comment')->middleware('logging');
Route::delete('/couses/comment/{id}/delete', 'couses\CommentCouseController@delete_comment')->middleware('admin');

Route::get('/Admin/rdr/{token}', 'redirect_from_front\Redirect_from_front_Controller@index');
