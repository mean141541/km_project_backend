@extends('layouts.layout') 
@section('content')

  <!-- Back Button -->
  <div class="header pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-3">
        <div class="col-lg-6 col-7">
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="{{route('user.index')}}" class="btn btn-sm btn-primary">กลับ</a>
        </div>
        </div>
    </div>
    </div>
</div>

<div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
<form class="form form-vertical" id="image-form" action="{{ route('user.insert') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-6 text-center">
            <div class="file-field">
                <div class="z-depth-1-half mb-2 ">
                    <img src="{{asset('/assets/img/icon/image1.png')}}" id="preview" class="img-thumbnail" width="150px">
                </div>
                <div class="d-flex justify-content-center">
                    <div class="btn btn-mdb-color btn-rounded float-left">
                        <input type="file" name="image" class="file" accept="image/*">
                    </div>
                    <div class="input-group my-1 col-md-6">
                        <input type="text" class="form-control form-control-sm" disabled placeholder="Upload File" id="file">
                        <div class="input-group-append">
                            <button type="button" class="browse btn btn-sm btn-success">Browse</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="user">ชื่อ</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="user">สกุล</span></label>
                        <input type="text" class="form-control" name="lastname" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="user">อีเมลล์</label>
                        <input type="text" class="form-control" name="email" required>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="user">รหัสผ่าน</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                </div>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="permission" id="PermissionUser" value="User" checked>
                <label class="form-check-label" for="PermissionUser">User</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="permission" id="PermissionAdmin" value="Admin">
                <label class="form-check-label" for="PermissionAdmin">Admin</label>
              </div>
            <div class="col-md-10">
                <div class="form-group">
                    <hr>
                    <div class="text-right">
                        <button type="submit" class="btn btn-sm btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script>
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
     // get loaded data and render thumbnail.
     document.getElementById("preview").src = e.target.result;
    };
   // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });
 </script>
@endsection

@endsection