@extends('layouts.layout')

@section('content')
<div class="row justify-content-center">
    <h1>ผู้ใช้งานระบบ</h1>
</div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="{{route('user.create')}}" class="btn btn-sm btn-primary">เพิ่มผู้ใช้งานระบบ</a>
            </div>
            </div>
        </div>
        </div>
    </div>

    {{-- Table --}}
    <div>
        <table class="table align-items-center">
            <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Avatar</th>
                    <th>Name</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>Permission</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody class="list">
                @foreach ($detailuser as $row)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td><img src={{asset($row['avatar'])}} class="avatar-pic" alt="avatar"></td>
                    <td>{{$row['name']}}</td>
                    <td>{{$row['lastname']}}</td>
                    <td>{{$row['email']}}</td>
                    <td>{{$row['permission']}}</td>
                    <td><a href="{{route('user.edit', $row['id'])}}" class="btn btn-sm btn-warning">แก้ไข </a>&nbsp;
                        <a href="{{route('user.delete', $row['id'])}}" class="btn btn-sm btn-danger" onclick="return confirm('ยืนยัน')">ลบ</a>
                    </td>
                </tr>


                @endforeach
            </tbody>
        </table>
    </div>
@endsection
