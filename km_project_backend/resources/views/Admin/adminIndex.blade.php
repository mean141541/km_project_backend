@extends('layouts.layout',['no_search' => false])
@section('content')
    <canvas id="myChart" style="max-width: 1000px;"></canvas>
    <div class="col-md-5">
        <canvas id="myChart"></canvas>
      </div>
  <!-- JQuery -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>
  <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["ผู้ใช้งาน", "แผนก", "ข่าวสารและกิจกรรม", "ท่องเที่ยว", "อบรม"],
                datasets: [{
                    label: 'จำนวนข้อมูลทั้งหมด',
                    data: [
                        {{count(App\User::all())}},
                        {{count(App\Department::all())}},
                        {{count(App\News::all())}},
                        {{count(App\Content::all())}},
                        {{count(App\Courses::all())}},
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                    ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                ],
                borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>

 @endsection
