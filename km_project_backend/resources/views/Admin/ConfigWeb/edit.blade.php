@extends('layouts.layout')
{{-- {{dd($config)}} --}}
@section('content')
<div class="row justify-content-center">
    <h1>Edit Config Web</h1>
</div>
<div class="header pb-6 text-center">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
        </div>
        </div>
    </div>
    </div>
</div>

<div class="container">
<form action="{{route('config.update', $config->id)}}" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="card shadow mb-5" style="padding: 40px 40px 40px 40px;">
        <div class="row" style="margin: auto">
            <div class="col">
                <div class="input-group mb-3">
                    <img src="{{asset($config->path_pic)}}" alt="" height="100">
                </div>
            </div>
        </div>
        <div class="row" style="margin: auto">
            <div class="col">
                <div class="input-group mb-3">
                    <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" name="image" accept="image/*">
                    <label class="custom-file-label" for="inputGroupFile01">เลือกรูป</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Address</span>
                    </div>
                    <textarea class="form-control" aria-label="With textarea" name="address" required >{{$config->address}}</textarea>
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Office_hour</span>
                    </div>
                    <input type="text" class="form-control" name="office_hour" value="{{$config->office_hour}}" required >
                </div>
            </div>
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Contact_Mail</span>
                    </div>
                    <input type="email" class="form-control" name="contect_mail" value="{{$config->contect_mail}}" required >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Contact_Phone</span>
                    </div>
                    <input type="text" class="form-control" name="contect_phone" value="{{$config->contect_phone}}" required >
                </div>
            </div>
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Status</span>
                    </div>
                    <select class="custom-select" id="inputGroupSelect01" name="status">
                        <option value="active" {{($config->status == 'active')? 'selected':''}}>active</option>
                        <option value="reject" {{($config->status == 'reject')? 'selected':''}}>reject</option>
                      </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col text-right">
                <button class="btn btn-sm btn-success">บันทึก</button>
            </div>
        </div>
    </div>
</form>
</div>
@endsection
