@extends('layouts.layout',['no_search' => false])

@section('content')
<div class="row justify-content-center">
    <h1>Config Web</h1>
</div>
    {{-- Insert Button --}}
    <div class="header pb-6 text-center">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            </div>
        </div>
        </div>
    </div>

    <div class="container">
        @if($errors->any())
            <div class="alert alert-success" role="alert">
                {{$errors->first()}}
            </div>
        @endif
        <div class="card shadow mb-5" style="padding: 40px 40px 40px 40px;">
            <div class="row" style="margin: auto">
                <div class="col">
                    <div class="input-group mb-3">
                        <img src="{{asset($config->path_pic)}}" alt="" height="100">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Address</span>
                        </div>
                        <textarea class="form-control" aria-label="With textarea" readonly>{{$config->address}}</textarea>
                      </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">Office_hour</span>
                        </div>
                        <input type="text" class="form-control" value="{{$config->office_hour}}" readonly>
                    </div>
                </div>
                <div class="col-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">Contact_Mail</span>
                        </div>
                        <input type="email" class="form-control" value="{{$config->contect_mail}}" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">Contact_Phone</span>
                        </div>
                        <input type="text" class="form-control" value="{{$config->contect_phone}}" readonly>
                    </div>
                </div>
                <div class="col-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">Status</span>
                        </div>
                        <input type="text" class="form-control" value="{{$config->status}}" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-right">
                    <a href="{{route('config.edit', $config->id)}}" class="btn btn-sm btn-warning">แก้ไข </a>
                </div>
            </div>
        </div>
    </div>
@endsection
