@extends('layouts.layout')

@section('content')

<center><h1> จัดการข้อมูล </h1></center>
<br>
{{-- {{action('course.update')}} --}}
{{-- {{route('course.update', $user[0]->id)}} --}}
{{-- {{action('AdminController@coursesupdate', $row->id)}} --}}
<div class="container">

    <form  action="{{route ('course.update', $courses->id)}}" enctype="multipart/form-data" method="POST" >
    {{csrf_field()}}
    @method('put')
      {{-- <div class="form-group">
        <label for="formGroupExampleInput text-dark ">รหัสชื่อผู้ใช้</label>
        <input type="hidden" class="form-control" name="user_id"  id="formGroupExampleInput" placeholder="กรอกรหัสผู้ใช้" value="5">
      </div> --}}

    <div class="form-group">
        <label for="formGroupExampleInput2">หัวข้อการอบรม</label>
    <input type="text" class="form-control" name="title"  id="formGroupExampleInput2" placeholder="หัวข้อการอบรม" value="{{$courses->title}}">

      </div>
    <div class="form-group">
      <label>รายละเอียด</label>
        <textarea class="form-control" rows="5" placeholder="รายละเอียด" name="detail"  value=""> {{$courses->detail}}</textarea>
        </div>

{{--
        <div class="form-group">
            <label for="inputState"  value=""> สถานะ </label>
            <select id="inputState" class="form-control" name="status" >
              <option @if($courses->status == 'active') selected @endif name="status" >active</option>
              <option @if($courses->status == 'reject') selected @endif name="status" >reject</option>
            </select>
        </div> --}}
        {{-- <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="deptStatus" id="StatusActive" value="active"
            {{ $dept[0]->status == 'active' ? 'checked' : '' }}>
            <label class="form-check-label" for="StatusActive">Active</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="deptStatus" id="StatusReject" value="reject"
            {{ $dept[0]->status == 'reject' ? 'checked' : '' }}>
            <label class="form-check-label" for="StatusReject">Reject</label>
          </div> --}}


        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="status"  id="StatusActive" value="active"
               {{ $courses->status == 'active' ? 'checked' : '' }} >

            <label class="form-check-label" for="StatusActive">Active</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="status"  id="StatusReject" value="reject"
            {{ $courses->status == 'reject' ? 'checked' : '' }}

            >
            <label class="form-check-label" for="StatusReject">Reject</label>
        </div>



<div>
    <img src="{{$courses->path_pic}}" class="rounded float-left" alt="..."  height="150" width="250">
</div>


     <div class="form-group">
     <input type="file" class="form-control-file"  id="exampleFormControlFile1" name="path_pic" >
      </div>


    <button type="submit" class="btn btn-success btn-sm" >บันทึกการเปลี่ยนแปลง</button>
    <a href="http://127.0.0.1:8000/Admin/course" class="btn btn-sm btn-primary">กลับ</a>
    </form>

    <br>




</div>

@endsection

