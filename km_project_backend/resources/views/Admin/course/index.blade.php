@extends('layouts.layout')

@section('content')


<h1> <center> อบรม <center> </h1><div align = 'right'>
     <a href="{{route('course.create')}}" class="btn btn-primary">เพิ่มข้อมูล</a>
     <a href="{{route('course.vedio')}}" class="btn btn-secondary">วิดีโอ</a>
    </div >

<br>


    <!-- Table -->
    <div>
        <table class="table ">
            <thead class="thead-light">
                <tr>
                    <th>ลำดับ</th>
                    {{-- <th>ID</th> --}}
                    {{--<th>รหัสผู้ให้</th>--}}
                    <th>หัวข้อการอบรม</th>
                    <th >รายละเอียด</th>
                    <th>รูปภาพ</th>
                    <th>สถานะ</th>
                    <th></th>
                </tr>
            </thead>



            @foreach ($courses as $row)
            <tbody class="list">
                <tr>
                    <td>{{$loop->iteration}}</td>
                    {{-- <td>{{$row->user_id}}</td>--}}
                    <td >{{$row->title}}</td>
                    <td height="150" width="500">{{$row->detail}}</td>
                    <td><center><img src="{{$row->path_pic}}" height="150" width="200"> <center></td>
                    <td>{{$row->status}}</td>

                    <td>
                            {{-- <a href="{{action('AdminController@coursedetail', $row->id)}}" class="btn btn-sm btn-success">รายละเอียด</a> --}}
                            <a href="{{route('course.edit', $row->id)}}" class="btn btn-warning"> จัดการ</a> &nbsp;

                            {{-- <a href="{{route('course.update', $row->id) }}" class="btn btn-sm btn-warning"> จัดการ</a> &nbsp;

                        <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal" v> จัดการ{{$row->id}} </button> --}}
                    </td>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
<tr>




