@extends('layouts.layout')

@section('content')
    <div class="row justify-content-center">
        <h1>ประเภทข่าวสารและกิจกรรม</h1>
    </div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="{{route('news_type.create')}}" class="btn btn-info">เพิ่มประเภท</a>
                <a href="{{route('news.index')}}" class="btn btn-primary">ข่าว</a>
            </div>
            </div>
        </div>
        </div>
    </div>

<div>
    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th>ID</th>
                <th>ชื่อประเภทข่าว</th>
                <th>สถานะ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="list">
            @foreach ($data->data as $row)
            <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->type_name}}</td>
                <td>{{$row->status}}</td>
                <td>
                    <a href="{{ route('news_type.detail', $row->id) }}" class="btn btn-warning">จัดการ</a>

                </td>
            </tr>

            @endforeach
        </tbody>
    </table>
</div>
@endsection
