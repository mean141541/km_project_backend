@extends('layouts.layout')
{{-- {{dd(asset($data[0]->path_pic))}} --}}
{{-- {{dd($data}} --}}
@section('content')
    <div class="row justify-content-center">
        <h1>อัลบั้มรูป</h1>
    </div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
            </div>
            </div>
        </div>
        </div>
    </div>

<div>
    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th>ID</th>
                <th>รูปภาพ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="list">
            @if ($data != [])
            @foreach ($data as $row)

            <tr>
                <td>{{$row->id}}</td>
                <td>
                    <img src="{{asset($row->path_pic)}}" onerror="this.onerror=null; this.src='{{asset('img/news/icon.webp')}}'" alt="" width="150px">
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="3">ไม่มีข้อมูลรูปภาพ</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
@endsection
