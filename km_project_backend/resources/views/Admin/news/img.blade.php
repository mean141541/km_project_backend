@extends('layouts.layout')
{{-- {{dd($data)}} --}}
@section('content')
    <div class="row justify-content-center">
        <h1>อัลบั้มรูป</h1>
    </div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5">
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6">
                    <form action="{{ route('news_album.insert',$id) }}" method="POST"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile02" name="image[]" multiple>
                                <label class="custom-file-label" for="inputGroupFile02">เลือกรูป</label>
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">เพิ่ม</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>

<div>
    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th>ID</th>
                <th>รูปภาพ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="list">
            @if ($data != [])
            @foreach ($data as $row)

            <tr>
                <td>{{$row->id}}</td>
                <td>
                    <img src="{{asset($row->path_pic)}}" onerror="this.onerror=null; this.src='{{asset('img/news/icon.webp')}}'" alt="" width="150px">
                </td>
                <td>


                    <div class="header pb-6">
                        <div class="container-fluid">
                        <div class="header-body">
                            <div class="row align-items-center py-4">
                            <div class="col-lg-6 col-7">
                            </div>
                            <div class="col-lg-6 col-5">
                                <div class="row">
                                    {{-- <div class="col-6"></div> --}}
                                    {{-- <div class="col-6"> --}}
                                        {{-- {{dd($row->id)}} --}}
                                    <form action="{{ route('news_album.edit',$row->id) }}" method="POST"  enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile02" name="image[]" multiple>
                                                <label class="custom-file-label" for="inputGroupFile02">เลือกรูป</label>
                                            </div>
                                            <div class="input-group-append">
                                                <button class="btn btn-warning" onclick="return confirm('ยืนยันการแก้ไข?')" type="submit">แก้ไข</button>
                                            </div>
                                        </div>
                                    </form>
                                    {{-- </div> --}}
                                </div>
                                <a href="{{ route('news_album.delete', $row->id) }}" onclick="return confirm('ยืนยันการลบ?')" class="btn btn-danger">ลบ</a>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>


                    {{-- <a href="{{ route('news.detail', $row->id) }}" class="btn btn-warning">เปลี่ยนรูป</a> --}}
                    {{-- <form action="{{ route('news.detail', $row->id) }}" method="POST"> --}}

                    {{-- </form> --}}
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="3">ไม่มีข้อมูลรูปภาพ</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
@endsection
