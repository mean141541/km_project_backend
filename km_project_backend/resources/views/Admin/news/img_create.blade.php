
        @extends('layouts.layout')
        {{-- {{dd($data)}} --}}
        @section('head')
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
        @endsection

        @section('content')
            <div class="row justify-content-center">
                <h2>เพิ่มรูปภาพ</h2>
            </div>
            {{-- Insert Button --}}
            <div class="header pb-6">
                <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        {{-- <a type="hidden" href="{{route('news.create')}}" class="btn btn-primary">New</a> --}}
                    </div>
                    </div>
                </div>
                </div>
            </div>

        <div class="container">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>เตือน</strong> กรุณากรอกข้อมูลให้ครบถ้วน<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            <form method="post" action="{{ route('news.album.update',$id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-6">
                        <br><br>
                        <div class="input-group mb-3">
                            <div class="input-group increment">
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="image" name="image[]" multiple>
                                  <label class="custom-file-label" for="image">เลือกไฟล์</label>
                                </div>
                                <div class="input-group-append">
                                  <button class="btn btn-outline-secondary" type="button">เพิ่ม</button>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-right">
                            <a onclick="return confirm('ยังไม่ได้บันทึกข้อมูล ยืนยันที่จะออกจากหน้านี้?')" href="{{route('news.album',$id)}}" class="btn btn-danger">ยกเลิก</a>
                            <button type="submit" onclick="return confirm('ยืนยัน')" class="btn btn-success">ยืนยัน</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @endsection

        @section('script')
        <script>
            $('#detail').summernote({
              placeholder: 'Detail',
              tabsize: 2,
              maxHeight: 1000,
              height: 300
            });
        </script>
        @endsection
