@extends('layouts.layout')

@section('head')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
@endsection
@section('content')
<div class="row justify-content-center">
    <h2>แก้ไขประเภทข่าวสาร</h2>
</div>
{{-- Insert Button --}}
<div class="header pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
        </div>
        <div class="col-lg-6 col-5 text-right">
            {{-- <a type="hidden" href="{{route('news.create')}}" class="btn btn-primary">New</a> --}}
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container">
    <form method="post" action="{{ route('news_type.insert') }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">ชื่อประเภทข่าว</span>
                    </div>
                    <input type="text" name="type_name" class="form-control">
                </div>
            </div>
        </div>
        <div class="text-right">
            <a href="{{route('news.type')}}" class="btn btn-danger">ย้อนกลับ</a>
            <button type="submit" class="btn btn-success">เพิ่ม</button>
        </div>
    </form>
</div>
@endsection
