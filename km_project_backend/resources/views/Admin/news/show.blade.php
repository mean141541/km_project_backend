@extends('layouts.layout')

@section('content')
    <div class="row justify-content-center">
        <h1>ข่าวสารและกิจกรรม</h1>
    </div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="{{route('news.create')}}" class="btn btn-info">เพิ่มข่าวสาร</a>
                <a href="{{route('news.type')}}" class="btn btn-primary">ประเภทข่าว</a>
            </div>
            </div>
        </div>
        </div>
    </div>

<div>
    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>ชื่อบทความ</th>
                <th>ประเภท</th>
                <th>สถานะ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="list">
            @foreach ($data->data as $row)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$row->title}}</td>
                <td>{{$row->type_name}}</td>
                <td>{{$row->status}}</td>
                <td>
                    <a href="{{ route('news.album', $row->id) }}" class="btn btn-light">อัลบั้มรูป</a>
                    <a href="{{ route('news.detail', $row->id) }}" class="btn btn-warning">จัดการ</a>
                    <a href="{{ route('news.delete', $row->id) }}" data-method="delete" class="btn btn-danger" onclick="return confirm('ยืนยัน')">ลบ</a>
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>
</div>
@endsection
