@extends('layouts.layout')

@section('content')

    <!-- Insert Button -->
    <div class="header pb-6">
        <div class="container-fluid">
          <h1 align="center">หลักสูตร</h1>
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="{{route('knowledge.create')}}" class="btn btn-md btn-info">เพิ่มข้อมูล</a>
            </div>
            </div>
        </div>
        </div>
    </div>

    <!-- Table -->
    <div>
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>ลำดับ</th>
                    <th>ไอดี</th>
                    <th>ชื่อวิชา</th>
                    <th>sub_subject</th>
                    <th>ประเภท</th>
                    <th>สถานะ</th>
                    <th>รายละเอียด</th>
                </tr>
            </thead>
            <tbody class="list">
              <?php $number = 1; ?>
                @foreach ($subject as $row)
                <tr>
                  <td>{{$number}}</td>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$row['name']}}</td>
                    <td></td>
                    {{-- <td><img src="{{asset($row['pic_path'])}}" alt="test"  width="100px"></td>  --}}
                    <td>{{$row->department['department_name']}}</td>
                    <td>{{$row['status']}}</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#exampleModal"> รายละเอียด </button>
                        <a href="" class="btn btn-sm btn-warning">แก้ไข </a> &nbsp;
                        <a href="" class="btn btn-sm btn-danger">ลบ</a>
                    </td>
                </tr>
                <?php $number++ ?>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- modal Detail -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">รายละเอียด</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>

@endsection
