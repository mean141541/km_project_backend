@extends('layouts.layout')

@section('content')

    <!-- Insert Button -->
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-3">
            <div class="col-lg-6 col-7">
                <h3>หมวดหมู่</h3>
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="{{route('dept.create')}}" class="btn btn-sm btn-primary">เพิ่มข้อมูล</a>
            </div>
            </div>
        </div>
        </div>
    </div>
    
    <!-- Table -->
    <div>
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>ไอดี</th>
                    <th>หมวดหมู่</th>
                    <th>รูปภาพ</th>
                    <th>สถานะ</th>
                    <th>รายละเอียด</th>
                </tr>
            </thead>
            <tbody class="list">
                @foreach ($dept as $row)
                <tr>
                    <td>{{$row->id}}</td>
                    <td><a href="{{route('subject.index', $row->id)}}">{{$row->department_name}}</a></td>
                    <td><img src="{{asset($row->pic)}}" alt="test"  width="100px"></td> 
                    <td>{{$row->status}}</td>
                    <td>
                        {{-- <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#exampleModal"> รายละเอียด </button> --}}
                        <a href="{{route('dept.edit', $row->id)}}" class="btn btn-sm btn-warning">แก้ไข </a>&nbsp; 
                        <a href="{{route('dept.delete', $row->id)}}" class="btn btn-sm btn-danger" onclick="return confirm('ยืนยัน')">ลบ</a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- modal Detail -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">รายละเอียด</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>

@endsection
