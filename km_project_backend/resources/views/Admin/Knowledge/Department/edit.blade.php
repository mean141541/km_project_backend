@extends('layouts.layout')

@section('content')

 <!-- Back Button -->
 <div class="header pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-3">
        <div class="col-lg-6 col-7">
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="{{route('dept.index')}}" class="btn btn-sm btn-primary">กลับ</a>
        </div>
        </div>
    </div>
    </div>
</div>

<div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
<form class="form form-vertical" id="image-form" action="{{ route('dept.update', $dept[0]->id)}}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-6 text-center">
            <div class="file-field">
                <div class="z-depth-1-half mb-2 ">
                    <img src="{{$dept[0]->pic}}" id="preview" class="img-thumbnail" width="150px">
                </div>
                <div class="d-flex justify-content-center">
                    <div class="btn btn-mdb-color btn-rounded float-left">
                        <input type="file" name="image" class="file" accept="image/*" value="{{$dept[0]->pic}}">
                    </div>
                    <div class="input-group my-1 col-md-6">
                        <input type="text" id="file" class="form-control form-control-sm" placeholder="Upload File" disabled>
                        <div class="input-group-append">
                            <button type="button" class="browse btn btn-sm btn-success" >Browse</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="department">ตำแหน่ง</label>
                        <input type="text" class="form-control" name="deptName" value="{{$dept[0]->department_name}}">
                    </div>
                </div>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="deptStatus" id="StatusActive" value="active" 
                {{ $dept[0]->status == 'active' ? 'checked' : '' }}>
                <label class="form-check-label" for="StatusActive">Active</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="deptStatus" id="StatusReject" value="reject"
                {{ $dept[0]->status == 'reject' ? 'checked' : '' }}>
                <label class="form-check-label" for="StatusReject">Reject</label>
              </div>
            <div class="col-md-10">
                <div class="form-group">
                    <hr>
                    <div class="text-right">
                        <button type="submit" class="btn btn-sm btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script>
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
     // get loaded data and render thumbnail.
     document.getElementById("preview").src = e.target.result;
    };
   // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    
 </script>
@endsection

@endsection