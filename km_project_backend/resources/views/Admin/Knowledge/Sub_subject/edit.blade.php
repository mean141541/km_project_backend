@extends('layouts.layout')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" >
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

@section('content')

  <!-- Back Button -->
  <div class="header pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-3">
        <div class="col-lg-6 col-7">
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="{{route('sub.index', $sub->subject_id)}}" class="btn btn-sm btn-primary">กลับ</a>
        </div>
        </div>
    </div>
    </div>
</div>

<div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
<form class="form form-vertical" id="image-form" action="{{route('sub.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6 text-center">
            <div class="file-field">
                <div class="z-depth-1-half mb-2 ">
                    <img src="{{asset('/assets/img/icon/image1.png')}}" id="preview" class="img-thumbnail" width="150px">
                </div>
                <div class="d-flex justify-content-center">
                    <div class="btn btn-mdb-color btn-rounded float-left">
                        <input type="file" id="image" name="image" class="file" accept="image/*">
                    </div>
                    <div class="input-group my-1 col-md-6">
                        <input type="text" class="form-control form-control-sm" disabled placeholder="Upload File" id="file">
                        <div class="input-group-append">
                            <button type="button" class="browse btn btn-sm btn-success">Browse</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="sub">รายวิชา</label>
                        <input name="subject_id" type="hidden" value="{{$sub->id}}">
                        {{-- <input name="subject_id" type="hidden" value="{{Route::current()->id}}"> --}}
                        {{-- <input type="text" class="form-control" name="" value="@if(isset(App\Subject::find(Route::current()->id)->get()->first()->name)) {{App\Subject::find(Route::current()->id)->get()->first()->name}} @endif" disabled> --}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="sub">ชื่อรายวิชาย่อย</span></label>
                    <input type="text" class="form-control" name="title" value="{{$sub->title}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="sub">Tag</label>
                        <input type="text" class="form-control" name="tag" value="{{$sub->tag}}">
                    </div>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="sub">รายละเอียด</span></label>
                        <textarea id="detail" name="detail" value>{{$sub->detail}}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="subjectStatus" id="StatusActive" value="active" checked>
                <label class="form-check-label" for="StatusActive">Active</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="subjectStatus" id="StatusReject" value="reject">
                <label class="form-check-label" for="StatusReject">Reject</label>
              </div>
            <div class="col-md-10">
                <div class="form-group">
                    <hr>
                    <div class="text-right">
                        <button type="submit" class="btn btn-sm btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");

    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
     // get loaded data and render thumbnail.
     document.getElementById("preview").src = e.target.result;
     document.getElementById("image").src = e.target.result;


    };
   // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $('#detail').summernote({
      placeholder: 'Detail',
      tabsize: 2,
      maxHeight: 500,
      height: 300
    });

 </script>
@endsection

@endsection
