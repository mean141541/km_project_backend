@extends('layouts.layout')

@section('content')
    <!-- Insert Button -->
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-3">
            <div class="col-lg-6 col-7">
                <h3>รายวิชาย่อย</h3>
            </div>
            {{-- {{dd($subject[0]->department_id)}} --}}
            <div class="col-lg-6 col-5 text-right">
                <a href="{{route('subject.index',$subject[0]->deptID)}}" class="btn btn-sm btn-primary">กลับ</a>
                <a href="{{route('sub.create', $sub[0]->subject_id)}}" class="btn btn-sm btn-primary">เพิ่มข้อมูล</a>
            </div>
            </div>
        </div>
        </div>
    </div>

    <!-- Table -->
    <div>
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>ไอดี</th>
                    <th>ชื่อรายวิชาย่อย</th>
                    <th>รูปภาพ</th>
                    <th>view</th>
                    <th>tag</th>
                    <th>รายละเอียด</th>
                </tr>
            </thead>
            <tbody class="list">
                @foreach ($sub as $row)
                <tr>
                    <td>{{$row->id}}</td>
                    <td>{{$row->title}}</td>
                    <td><img src="{{asset($row->pic_path)}}" alt="test"  width="100px"></td>
                    <td>{{$row->view}}</td>
                    <td>{{$row->tag}}</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#view_{{$row->id}}" > รายละเอียด </button>
                        <a href="{{route('sub.edit', $row->id)}}" class="btn btn-sm btn-warning">แก้ไข </a>&nbsp;
                        <a href="{{ route('sub.delete', $row->id) }}" data-method="DELETE" class="btn btn-danger" onclick="return confirm('ยืนยัน')">ลบ</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- modal Detail -->
    @foreach ($sub as $row)
    <div class="modal fade bd-example-modal-lg" id="view_{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">{{$row->title}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th>รายละเอียด</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        <tr>
                            <td>{!!$row->detail!!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
          </div>
        </div>
      </div>
      @endforeach

@section('script')
    <script>

    </script>
@endsection

@endsection
