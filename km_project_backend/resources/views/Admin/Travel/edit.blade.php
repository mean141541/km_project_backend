
@extends('layouts.layout')
  {{-- {{dd($data)}} --}}
@section('head')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
@endsection

@section('content')
    <div class="row justify-content-center">
        <h2>จัดการสถานที่ท่องเที่ยว</h2>
    </div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
                {{-- <a type="hidden" href="{{route('news.create')}}" class="btn btn-primary">New</a> --}}
            </div>
            </div>
        </div>
        </div>
    </div>

<div class="container">
    <form action = "/api/content/update/{{$data->id}}" method = "post">
        {{-- <form method="post" action="{{ route('Travel.Update'{{$data->id}}) }}"> --}}
        {{ csrf_field() }}
        @method('put')

        <div class="row">
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">สถานที่ท่องเที่ยว</span>
                    </div>
                <input value="{{$data->title}}" type="text" name="title" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-append">
                        <label class="input-group-text">ประเภท</label>
                    </div>
                    {{-- {{dd($data)}} --}}
                    <select class="custom-select" name="type_id">
                        {{-- @foreach ($type as $row)
                            <option value="{{$row->id}}" {{$row->id === $data->type_id ? 'selected' : ''}}>{{$row->name}}</option>
                        @endforeach --}}
                        @foreach ($type as $row)
                            <option value="{{$row->id}}" {{$row->id === $data->type_id ? 'selected' : ''}}>{{$row->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
</div>

    <div class="form-group">
            <textarea id="detail" name="detail" va>{{$data->detail}}</textarea>
            {{-- <label>รายละเอียด</label>
            <textarea class="form-control" rows="5" placeholder="รายละเอียด" name="detail"> va>{{$data->detail}}</textarea>
            --}}
    </div>

    <div class="row">
            <div class="col-9"></div>
            <div class="col-3 text-right">
                {{-- <div class="col-6"> --}}
                <div class="input-group mb-3">
                    <select class="custom-select" name="status">
                        <option {{$data->status === 'active' ? 'selected' : ''}} value="active">active</option>
                        <option {{$data->status === 'reject' ? 'selected' : ''}} value="reject">reject</option>
                    </select>
                    <div class="input-group-append">
                        <span class="input-group-text">สถานะ</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="text-right">
            <a href="{{route('Travel.index')}}" class="btn btn-danger">ย้อนกลับ</a>
            <button type="submit" class="btn btn-success">แก้ไข</button>
        </div>
        
@endsection

@section('script')
<script>
    $('#detail').summernote({
      placeholder: '',
      tabsize: 2,
      maxHeight: 1000
    });
  </script>
@endsection

