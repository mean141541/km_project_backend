@extends('layouts.layout')
{{-- {{dd($data)}} --}}
@section('content')
    <div class="row justify-content-center">
        <h1>ท่องเที่ยว</h1>
    </div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="{{route('Travel.create')}}" class="btn btn-info">เพิ่มสถานที่ท่องเที่ยว</a>
                {{-- <a href="{{route('news.type')}}" class="btn btn-primary">ประเภทข่าว</a> --}}
            </div>
            </div>
        </div>
        </div>
    </div>

<div>
    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>ประเภท</th>
                <th>สถานะ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="list">
            @foreach ($data as $row)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$row->title}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->status}}</td>
                <td>
                    <a href="{{ route('Travel.album', $row->id) }}" class="btn btn-light">อัลบั้มรูป</a>
                    <a href="{{ route('Travel.edit', ['id'=>$row->id]) }}" class="btn btn-warning">จัดการ</a>
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>
</div>
@endsection
