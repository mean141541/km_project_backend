@extends('layouts.layout')
{{-- {{dd(asset($data[0]->path_pic))}} --}}
{{-- {{dd($data)}} --}}
@section('content')
{{-- <form action = "/api/content/image/uplode/{{$data->id}}" method = "post"> --}}
    {{ csrf_field() }}
    <div class="row justify-content-center">
        <h1>อัลบั้มรูป</h1>
    </div>
    {{-- Insert Button --}}
    <div class="header pb-6">
        <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            </div>
            <div class="col-lg-6 col-5 text-right">
            </div>
            </div>
        </div>
        </div>
    </div>

<div>


    {{-- <div class="container">
        <form action = "/api/content/update/{{$data->id}}" method = "post"> --}}
    {{-- <div class="text-right">
        <a href="{{route('Travel.index')}}" class="btn btn-danger">ย้อนกลับ</a>
        <button type="submit" class="btn btn-success">แก้ไข</button>
    </div>
</form> --}}

    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th>ID</th>
                <th>รูปภาพ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="list">
            @if ($data != [])
            @foreach ($data as $row)

            <tr>
                <td>{{$row->content_id}}</td>
                <td>
                    <img src="{{asset($row->path_pic)}}" onerror="this.onerror=null; this.src='{{asset('img/news/icon.webp')}'" alt="" width="300px">

                </td>

                <td>
                    <form action="">
                        <div class="form-group">
                            <br>
                            <label for="exampleFormControlFile1">อัพโหลดแก้ไขรูปภาพ</label>
                            <input type="file" class="form-control-file"  id="exampleFormControlFile1" name="image[]"multiple>
                            <br>
                            <button type="submit" class="btn btn-success">แก้ไข</button>
                        </div>
                    </form>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="3">ไม่มีข้อมูลรูปภาพ</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
@endsection
