<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CouseTableLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('couse_comment_log')) {


            Schema::create('couse_comment_log', function (Blueprint $table) {
                $table->id();
                $table->integer('user_id');
                $table->integer('couse_comment_id');
                $table->char('action',100);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couse_comment_log');
    }
}
