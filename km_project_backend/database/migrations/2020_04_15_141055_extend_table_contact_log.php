<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExtendTableContactLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_log', function (Blueprint $table) {
            DB::statement('ALTER TABLE config_log MODIFY COLUMN action VARCHAR(5000)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_log', function (Blueprint $table) {
            //
        });
    }
}
