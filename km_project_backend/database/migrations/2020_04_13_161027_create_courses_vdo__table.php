<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesVdoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_vdo', function (Blueprint $table) {
            $table->id();
            $table->integer('course_id');
            $table->char('title',100);
            $table->string('detail',100);
            $table->char('path_vdo',255);
            $table->char('status',10);
            $table->integer('view');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_vdo');
    }
}
