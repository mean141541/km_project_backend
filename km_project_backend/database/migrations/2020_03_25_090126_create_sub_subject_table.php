<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_subject', function (Blueprint $table) {
            $table->id();
            $table->integer('subject_id');
            $table->char('title',200);
            $table->char('pic_path',100);
            $table->longText('detail');
            $table->integer('view');
            $table->mediumText('tag');
            $table->timestamps();
            $table->char('status',10);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_subject');
    }
}
